<?php

use App\Http\Controllers\Test\TestController;
use App\Http\Controllers\Konter\KonterController;
use App\Http\Controllers\LandingPage\LandingPageController;
use App\Http\Controllers\Logout\LogoutController;
use App\Http\Controllers\Region\DistrictController;
use App\Http\Controllers\Region\ProvinceController;
use App\Http\Controllers\Region\RegencyController;
use App\Http\Controllers\Region\VillageController;
use App\Http\Livewire\Back\Dashboard\DashboardPage;
use App\Http\Livewire\Back\Image\CreateImagePage;
use App\Http\Livewire\Back\Image\EditImagePage;
use App\Http\Livewire\Back\Image\ImagePage;
use App\Http\Livewire\Back\Login\LoginPage;
use App\Http\Livewire\Back\Pos\CreatePosPage;
use App\Http\Livewire\Back\Pos\EditPosPage;
use App\Http\Livewire\Back\Pos\PosPage;
use App\Http\Livewire\Back\Region\CreateRegionPage;
use App\Http\Livewire\Back\Region\EditRegionPage;
use App\Http\Livewire\Back\Region\Partials\CityPage;
use App\Http\Livewire\Back\Region\Partials\DistrictPage;
use App\Http\Livewire\Back\Region\Partials\ProvincePage;
use App\Http\Livewire\Back\Region\Partials\VillagePage;
use App\Http\Livewire\Back\Region\RegionPage;
use App\Http\Livewire\Back\Setting\CreateSettingPage;
use App\Http\Livewire\Back\Setting\EditSettingPage;
use App\Http\Livewire\Back\Setting\SettingPage;

use App\Http\Livewire\Back\User\UserPage;
use App\Http\Livewire\Back\User\CreateUserPage;
use App\Http\Livewire\Back\User\EditUserPage;

use App\Http\Livewire\Back\Role\RolePage;
use App\Http\Livewire\Back\Role\CreateRolePage;
use App\Http\Livewire\Back\Role\EditRolePage;

use App\Http\Livewire\Front\CoomingSoon\CoomingSoonPage;
use App\Http\Livewire\Front\Landing\LandingPage;
use Illuminate\Support\Facades\Route;


// Route::get('/', LandingPage::class)->name('landing-page');
Route::get('/', CoomingSoonPage::class)->name('comingsoon');
Route::get('/maps', LandingPageController::class)->name('landing-page');
Route::post('/getRegencys', [LandingPageController::class, 'getRegencies']);
Route::post('/getDistricts', [LandingPageController::class, 'getDistricts']);
Route::post('/getVillages', [LandingPageController::class, 'getVillages']);
Route::post('/getPosCodeVill', [LandingPageController::class, 'getPosCodeVill']);
Route::post('/getProvGeoJSON', [LandingPageController::class, 'getProvGeoJSON']);
Route::post('/getRegGeoJSON', [LandingPageController::class, 'getRegGeoJSON']);
Route::post('/getDisGeoJSON', [LandingPageController::class, 'getDisGeoJSON']);
Route::post('/getVilGeoJSON', [LandingPageController::class, 'getVilGeoJSON']);
Route::post('/getGeoJSON', [LandingPageController::class, 'getGeoJSON']);
Route::post('/getKonterGeoJSON', [LandingPageController::class, 'getKonterGeoJSON']);
Route::post('/getUrlDownload', [LandingPageController::class, 'getUrlDownload']);
Route::post('/download', [LandingPageController::class, 'downloadImg']);
Route::post('/downloadVill', [LandingPageController::class, 'downloadImgVill']);
Route::get('/download-image/{image}', [LandingPageController::class, 'downloadImages']);


//Logout
Route::get('logout', [LogoutController::class, 'logout'])->name('logout');

Route::get('/back-office/auth', LoginPage::class)->name('login');

Route::group(['middleware' => 'auth'], function () {
    Route::group(['prefix' => 'back-office', 'as' => 'back-office.'], function () {

        // Dashboard
        Route::get('/', DashboardPage::class)->name('dashboard');

        // User
        Route::group(['prefix' => 'user', 'as' => 'user.'], function () {
            Route::get('/', UserPage::class)->name('index')->middleware('permission:View User');
            Route::get('/create', CreateUserPage::class)->name('create')->middleware('permission:Add User');
            Route::get('/edit/{id}', EditUserPage::class)->name('edit')->middleware('permission:Edit User');
        });

        // Role
        Route::group(['prefix' => 'role', 'as' => 'role.'], function () {
            Route::get('/', RolePage::class)->name('index')->middleware('permission:View Role');
            Route::get('/create', CreateRolePage::class)->name('create')->middleware('permission:Add Role');
            Route::get('/edit/{id}', EditRolePage::class)->name('edit')->middleware('permission:Edit Role');
        });

        // Region
        Route::group(['prefix' => 'region', 'as' => 'region.'], function () {
            Route::get('/province', ProvincePage::class)->name('province')->middleware('permission:View Provinsi');
            Route::get('/create-province', [ProvinceController::class, 'create'])->name('create-province')->middleware('permission:Add Provinsi');
            Route::post('/store-province', [ProvinceController::class, 'store'])->name('store-province')->middleware('permission:Add Provinsi');
            Route::get('/edit-province/{id}', [ProvinceController::class, 'edit'])->name('edit-province')->middleware('permission:Edit Provinsi');
            Route::post('/update-province/{id}', [ProvinceController::class, 'update'])->name('update-province')->middleware('permission:Edit Provinsi');

            Route::get('/city', CityPage::class)->name('city')->middleware('permission:View Kota/Kabupaten');
            Route::get('/create-regency', [RegencyController::class, 'create'])->name('create-regency')->middleware('permission:Add Kota/Kabupaten');
            Route::post('/store-regency', [RegencyController::class, 'store'])->name('store-regency')->middleware('permission:Add Kota/Kabupaten');
            Route::get('/edit-regency/{id}', [RegencyController::class, 'edit'])->name('edit-regency')->middleware('permission:Edit Kota/Kabupaten');
            Route::post('/update-regency/{id}', [RegencyController::class, 'update'])->name('update-regency')->middleware('permission:Edit Kota/Kabupaten');

            Route::get('/district', DistrictPage::class)->name('district')->middleware('permission:View Kecamatan');
            Route::get('/create-district', [DistrictController::class, 'create'])->name('create-district')->middleware('permission:Add Kecamatan');
            Route::post('/store-district', [DistrictController::class, 'store'])->name('store-district')->middleware('permission:Add Kecamatan');
            Route::get('/edit-district/{id}', [DistrictController::class, 'edit'])->name('edit-district')->middleware('permission:Edit Kecamatan');
            Route::post('/update-district/{id}', [DistrictController::class, 'update'])->name('update-district')->middleware('permission:Edit Kecamatan');

            Route::get('/village', VillagePage::class)->name('village')->middleware('permission:View Kelurahan');
            Route::get('/create-village', [VillageController::class, 'create'])->name('create-village')->middleware('permission:Add Kelurahan');
            Route::post('/store-village', [VillageController::class, 'store'])->name('store-village')->middleware('permission:Add Kelurahan');
            Route::get('/edit-village/{id}', [VillageController::class, 'edit'])->name('edit-village')->middleware('permission:Edit Kelurahan');
            Route::post('/update-village/{id}', [VillageController::class, 'update'])->name('update-village')->middleware('permission:Edit Kelurahan');
        });

        // Pos
        Route::group(['prefix' => 'pos', 'as' => 'pos.'], function () {
            Route::get('/', PosPage::class)->name('index')->middleware('permission:View Konter');
            Route::get('/create', [KonterController::class, 'create'])->name('create')->middleware('permission:Add Konter');
            Route::post('/store', [KonterController::class, 'store'])->name('store')->middleware('permission:Add Konter');
            Route::get('/edit/{id}', [KonterController::class, 'edit'])->name('edit')->middleware('permission:Edit Konter');
            Route::post('/update/{id}', [KonterController::class, 'update'])->name('update')->middleware('permission:Edit Konter');
            Route::get('/download/{id}', [KonterController::class, 'download'])->name('download')->middleware('permission:View Konter');
        });

        Route::group(['prefix' => 'konter', 'as' => 'konter.'], function () {
            Route::post('/getRegencys', [KonterController::class, 'getRegencies']);
            Route::post('/getDistricts', [KonterController::class, 'getDistricts']);
            Route::post('/getVillages', [KonterController::class, 'getVillages']);
            Route::get('/download/{geojson}', [KonterController::class, 'download']);
        });
    });
});

<?php

namespace Database\Seeders;

use App\Models\Role;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::statement('SET FOREIGN_KEY_CHECKS=0');

        User::truncate();

        $roleAdmin = Role::whereName('administrator')->first();

        $admin = User::create([
            'name' => 'Administrator',
            'email' => 'admin@admin.com',
            'password' => \Hash::make('123123'),
        ]);
        $admin->assignRole($roleAdmin);

        \DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}

<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Database\Seeders\IndoRegionRegencySeeder;
use Database\Seeders\IndoRegionVillageSeeder;
use Database\Seeders\IndoRegionDistrictSeeder;
use Database\Seeders\IndoRegionProvinceSeeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \DB::statement('SET FOREIGN_KEY_CHECKS=0');

        $this->call([
            // IndoRegionProvinceSeeder::class,
            // IndoRegionRegencySeeder::class,
            // IndoRegionDistrictSeeder::class,
            // IndoRegionVillageSeeder::class,
            RoleAndPermissionSeeder::class,
            UsersTableSeeder::class,
        ]);

        \DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}

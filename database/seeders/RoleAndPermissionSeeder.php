<?php

namespace Database\Seeders;

use App\Models\Permission;
use App\Models\Role;
use Illuminate\Database\Seeder;
use Spatie\Permission\PermissionRegistrar;

class RoleAndPermissionSeeder extends Seeder
{

    // use HasUuid;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        \DB::statement('SET FOREIGN_KEY_CHECKS=0');

        Permission::truncate();
        Role::truncate();

        app()[PermissionRegistrar::class]->forgetCachedPermissions();

        Permission::create(['name' => 'View User', 'guard_name' => 'web']);
        Permission::create(['name' => 'Add User', 'guard_name' => 'web']);
        Permission::create(['name' => 'Edit User', 'guard_name' => 'web']);
        Permission::create(['name' => 'Delete User', 'guard_name' => 'web']);

        Permission::create(['name' => 'View Role', 'guard_name' => 'web']);
        Permission::create(['name' => 'Add Role', 'guard_name' => 'web']);
        Permission::create(['name' => 'Edit Role', 'guard_name' => 'web']);
        Permission::create(['name' => 'Delete Role', 'guard_name' => 'web']);

        Permission::create(['name' => 'View Provinsi', 'guard_name' => 'web']);
        Permission::create(['name' => 'Add Provinsi', 'guard_name' => 'web']);
        Permission::create(['name' => 'Edit Provinsi', 'guard_name' => 'web']);
        Permission::create(['name' => 'Delete Provinsi', 'guard_name' => 'web']);

        Permission::create(['name' => 'View Kota/Kabupaten', 'guard_name' => 'web']);
        Permission::create(['name' => 'Add Kota/Kabupaten', 'guard_name' => 'web']);
        Permission::create(['name' => 'Edit Kota/Kabupaten', 'guard_name' => 'web']);
        Permission::create(['name' => 'Delete Kota/Kabupaten', 'guard_name' => 'web']);

        Permission::create(['name' => 'View Kecamatan', 'guard_name' => 'web']);
        Permission::create(['name' => 'Add Kecamatan', 'guard_name' => 'web']);
        Permission::create(['name' => 'Edit Kecamatan', 'guard_name' => 'web']);
        Permission::create(['name' => 'Delete Kecamatan', 'guard_name' => 'web']);

        Permission::create(['name' => 'View Kelurahan', 'guard_name' => 'web']);
        Permission::create(['name' => 'Add Kelurahan', 'guard_name' => 'web']);
        Permission::create(['name' => 'Edit Kelurahan', 'guard_name' => 'web']);
        Permission::create(['name' => 'Delete Kelurahan', 'guard_name' => 'web']);

        Permission::create(['name' => 'View Konter', 'guard_name' => 'web']);
        Permission::create(['name' => 'Add Konter', 'guard_name' => 'web']);
        Permission::create(['name' => 'Edit Konter', 'guard_name' => 'web']);
        Permission::create(['name' => 'Delete Konter', 'guard_name' => 'web']);

        $super_administrator = Role::create(['name' => 'administrator', 'guard_name' => 'web']);
        $super_administrator->syncPermissions(Permission::all());
    }
}

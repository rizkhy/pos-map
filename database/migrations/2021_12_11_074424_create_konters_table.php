<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKontersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('konters', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('postalcode');
            $table->integer('province_code');
            $table->integer('regency_code');
            $table->integer('district_code');
            $table->bigInteger('village_code');
            $table->string('image')->nullable();
            $table->string('geojson')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('konters');
    }
}

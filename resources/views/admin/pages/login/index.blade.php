<div class="login login-4 login-signin-on d-flex flex-row-fluid" id="kt_login">
    <div class="d-flex flex-center flex-row-fluid bgi-size-cover bgi-position-top bgi-no-repeat"
        style="background-image: url({{ asset('back/media/bg/bg-3.jpg') }});">
        <div class="login-form text-center p-7 position-relative overflow-hidden">
            <!--begin::Login Header-->
            <div class="d-flex flex-center mb-15">
                <a href="#">
                    <img src="{{ asset('back/media/logos/logo-kominfo.png') }}" class="w-100px" alt="" />
                </a>
            </div>
            <!--end::Login Header-->
            <!--begin::Login Sign in form-->
            <div class="login-signin">
                <div class="mb-20">
                    <h3 class="opacity-40 font-weight-normal">Sign In To Admin</h3>
                    <p class="opacity-40">Enter your details to login to your account:</p>
                </div>
                <form class="form" wire:submit.prevent="doLogin">
                    <div class="form-group">
                        <input
                            class="form-control h-auto form-control-solid py-4 px-8 @error('email') is-invalid @enderror"
                            type="text" placeholder="Email" wire:model="email" name="username" autocomplete="off" />
                        @error('email')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <input
                            class="form-control h-auto form-control-solid py-4 px-8 @error('password') is-invalid @enderror"
                            type="password" placeholder="Password" wire:model="password" name="password" />
                        @error('password')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                    {{-- <div class="form-group">
                        <div class="captcha">
                            <span>{!! captcha_img() !!}</span>
                            <button type="button" class="btn btn-primary" class="reload" id="reload">
                                &#x21bb;
                            </button>
                        </div>
                    </div>
                    <div class="form-group">
                        <input
                            class="form-control h-auto form-control-solid py-4 px-8 @error('captcha') is-invalid @enderror"
                            type="text" placeholder="Captcha" wire:model="captcha" id="captcha" name="captcha"
                            autocomplete="off" />
                        @error('captcha')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div> --}}

                    <div class="form-group text-center mt-10">
                        <button type="submit" class="btn btn-pill btn-primary px-15 py-3" wire:target="doLogin"
                            wire:loading.attr="disabled">
                            <span wire:target="doLogin" wire:loading.remove>Sign In</span>
                            <span wire:target="doLogin" wire:loading>Please Wait...</span>
                        </button>
                    </div>
                </form>
            </div>
            <!--end::Login Sign in form-->
        </div>
    </div>
</div>

<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-6 subheader-solid" id="kt_subheader">
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-1">
                <!--begin::Page Heading-->
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <!--begin::Page Title-->
                    <h5 class="text-dark font-weight-bold my-1 mr-5">Manage User</h5>
                    <!--end::Page Title-->
                    <!--begin::Breadcrumb-->
                    <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                        <li class="breadcrumb-item">
                            <a href="" class="text-muted">User</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="" class="text-muted">Role</a>
                        </li>
                    </ul>
                    <!--end::Breadcrumb-->
                </div>
                <!--end::Page Heading-->
            </div>
            <!--end::Info-->
        </div>
    </div>
    <!--end::Subheader-->
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <!--begin::Card-->
            <div class="card card-custom">
                <div class="card-header flex-wrap border-0 pt-6 pb-0">
                    <div class="card-title">
                        <h3 class="card-label">
                            Edit Role
                        </h3>
                    </div>
                </div>
                <div class="card-body">

                    <form wire:submit.prevent="saved" class="form">
                        @csrf
                        <div class="card-body">
                            <div class="form-group row">
                                <label class="col-3 col-form-label" for="name">
                                    Name
                                    <span class="text-danger">*</span>
                                </label>
                                <div class="col-9">
                                    <input type="text"
                                        class="form-control form-control-lg @error('name') is-invalid @enderror"
                                        wire:model="name" placeholder="Please Enter Name">
                                    @error('name')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>

                            </div>

                            <div class="form-group row">
                                <label class="col-3 col-form-label" for="permission">
                                    Permission
                                    <span class="text-danger">*</span>
                                </label>
                                <div class="col-9">
                                    <div class="checkbox-list">
                                        <label class="checkbox checkbox-square">
                                            <input id="selectAll" type="checkbox" name="all" wire:click="checkAll"
                                                wire:model="all">
                                            <span style="background-color: rgb(84, 187, 84)"></span>
                                            All
                                        </label>
                                        @foreach ($listPermission as $permissions)
                                            <label class="checkbox checkbox-square">
                                                <input wire:model="permission.{{ $permissions['name'] }}.name"
                                                    type="checkbox" value="{{ $permissions['name'] }}">
                                                <span></span>
                                                {{ $permissions['name'] }}
                                            </label>
                                        @endforeach
                                    </div>
                                    @error('permission')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="form__actions">
                                <button type="reset" onclick="history.back()" class="btn btn-secondary">Cancel</button>
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
            <!--end::Card-->
        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>

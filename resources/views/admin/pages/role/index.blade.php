<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-6 subheader-solid" id="kt_subheader">
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-1">
                <!--begin::Page Heading-->
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <!--begin::Page Title-->
                    <h5 class="text-dark font-weight-bold my-1 mr-5">Manage User</h5>
                    <!--end::Page Title-->
                    <!--begin::Breadcrumb-->
                    <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                        <li class="breadcrumb-item">
                            <a href="" class="text-muted">User</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="" class="text-muted">Role</a>
                        </li>
                    </ul>
                    <!--end::Breadcrumb-->
                </div>
                <!--end::Page Heading-->
            </div>
            <!--end::Info-->
        </div>
    </div>
    <!--end::Subheader-->
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <!--begin::Card-->
            <div class="card card-custom">
                <div class="card-header flex-wrap border-0 pt-6 pb-0">
                    <div class="card-title">
                        <h3 class="card-label">
                            Role
                        </h3>
                    </div>
                    <div class="card-toolbar">
                        @can('Add Role')
                            <a href="{{ route('back-office.role.create') }}"
                                class="btn btn-primary font-weight-bolder btn-xs">
                                <i class="fas fa-plus-circle"></i>
                                Add Role
                            </a>
                        @endcan
                    </div>
                </div>

                <div class="card-body">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th class="text-center">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($this->roles as $role)
                                <tr>
                                    <td style="vertical-align: middle">{{ $role->name }}</td>
                                    <td class="text-center" style="vertical-align: middle">
                                        @can('Edit Role')
                                            <a href="{{ route('back-office.role.edit', $role->id) }}"
                                                data-toggle="tooltip" title="Edit" alt="Edit"
                                                class="btn btn-sm btn-warning btn-icon">
                                                <i class="far fa-edit"></i>
                                            </a>
                                        @endcan
                                        @can('Delete Role')
                                            <button type="button" wire:click="deleteConfirm('{{ $role->id }}')"
                                                data-toggle="tooltip" title="Delete" alt=" Delete"
                                                class="btn btn-sm btn-danger btn-icon">
                                                <i class="far fa-trash-alt"></i>
                                            </button>
                                        @endcan
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="7" class="text-center">
                                        Belum Ada Data
                                    </td>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                    <hr>
                    {{ $this->roles->links() }}
                </div>
            </div>
        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>

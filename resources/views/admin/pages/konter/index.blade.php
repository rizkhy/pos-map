@extends('admin.layout.master')
@section('content')
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <!--begin::Subheader-->
        <div class="subheader py-2 py-lg-6 subheader-solid" id="kt_subheader">
            <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center flex-wrap mr-1">
                    <!--begin::Page Heading-->
                    <div class="d-flex align-items-baseline flex-wrap mr-5">
                        <!--begin::Page Title-->
                        <h5 class="text-dark font-weight-bold my-1 mr-5">Manage Content</h5>
                        <!--end::Page Title-->
                        <!--begin::Breadcrumb-->
                        <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">

                            <li class="breadcrumb-item">
                                <a href="" class="text-muted">Konter</a>
                            </li>
                        </ul>
                        <!--end::Breadcrumb-->
                    </div>
                    <!--end::Page Heading-->
                </div>
                <!--end::Info-->
            </div>
        </div>
        <!--end::Subheader-->
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class="container">
                <!--begin::Card-->
                <div class="card card-custom">
                    <div class="card-header flex-wrap border-0 pt-6 pb-0">
                        <div class="card-title">
                            <h3 class="card-label">
                                Konter
                            </h3>
                        </div>
                        <div class="card-toolbar">
                            <a href="{{ route('back-office.konter.create') }}"
                                class="btn btn-primary font-weight-bolder btn-xs">
                                <i class="fas fa-plus-circle"></i>
                                Tambah
                            </a>
                        </div>
                    </div>

                    <div class="card-body">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>Image</th>
                                    <th>Kode Pos</th>
                                    <th>Unduh GeoJSON</th>
                                    <th class="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse($konters as $item)
                                    <tr>
                                        <td style="vertical-align: middle">
                                            <img src="/konterImg/{{ $item->image }}" width="100px">
                                        </td>
                                        <td style=" vertical-align: middle">{{ $item->postalcode }}
                                        </td>
                                        <td style="vertical-align: middle">
                                            <a href="{{ url('/back-office/konter/download', $item->geojson) }}">
                                                <button type="button" title="Download" alt=" Download"
                                                    class="btn btn-sm btn-success">
                                                    Unduh
                                                </button>
                                            </a>
                                        </td>
                                        <td class="text-center" style="vertical-align: middle">
                                            <a href="/back-office/konter/{{ $item->id }}/edit" title="Edit" alt="Edit"
                                                class="btn btn-sm btn-warning btn-icon">
                                                <i class="far fa-edit"></i>
                                            </a>
                                            <form action="/back-office/konter/{{ $item->id }}" method="POST">
                                                @csrf
                                                @method('DELETE')
                                                <button title="Delete" alt=" Delete" class="btn btn-sm btn-danger btn-icon">
                                                    <i class="far fa-trash-alt"></i></button>
                                            </form>
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="5" class="text-center">
                                            Belum Ada Data
                                        </td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                        <hr>
                        {{ $konters->links() }}
                    </div>
                </div>
            </div>
            <!--end::Container-->
        </div>
        <!--end::Entry-->
    </div>
@endsection

@extends('admin.layout.master')
@section('content')
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <!--begin::Subheader-->
        <div class="subheader py-2 py-lg-6 subheader-solid" id="kt_subheader">
            <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center flex-wrap mr-1">
                    <!--begin::Page Heading-->
                    <div class="d-flex align-items-baseline flex-wrap mr-5">
                        <!--begin::Page Title-->
                        <h5 class="text-dark font-weight-bold my-1 mr-5">Manage Content</h5>
                        <!--end::Page Title-->
                        <!--begin::Breadcrumb-->
                        <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">

                            <li class="breadcrumb-item">
                                <a href="" class="text-muted">Konter</a>
                            </li>
                        </ul>
                        <!--end::Breadcrumb-->
                    </div>
                    <!--end::Page Heading-->
                </div>
                <!--end::Info-->
            </div>
        </div>
        <!--end::Subheader-->
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class="container">
                <!--begin::Card-->
                <div class="card card-custom">
                    <div class="card-header flex-wrap border-0 pt-6 pb-0">
                        <div class="card-title">
                            <h3 class="card-label">
                                Edit Konter
                            </h3>
                        </div>
                    </div>
                    <div class="card-body">
                        <form action="{{ route('back-office.pos.update', $konters->id) }}" class="form"
                            enctype="multipart/form-data" method="POST">
                            @csrf
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-6">
                                        <div class="form-group row">
                                            <label class="col-4 col-form-label" for="postalcode">
                                                Kode Pos
                                                <span class="text-danger">*</span>
                                            </label>
                                            <div class="col-8">
                                                <input type="text"
                                                    class="form-control form-control-lg @error('postalcode') is-invalid @enderror"
                                                    name="postalcode" placeholder="Masukan kode pos" autocomplete="off"
                                                    value="{{ $konters->postalcode }}" />
                                                @error('postalcode')
                                                    <div class="invalid-feedback">
                                                        {{ $message }}
                                                    </div>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6">
                                        <div class="form-group row">
                                            <label class="col-4 col-form-label" for="province_code">
                                                Provinsi
                                                <span class="text-danger">*</span>
                                            </label>
                                            <div class="col-8">
                                                <select name="province_code" id="select_province"
                                                    class="form-control form-control-lg @error('province_code') is-invalid @enderror"
                                                    value="{{ $konters->province }}">
                                                    <option value="">- Provinsi -</option>
                                                    @foreach ($provinces as $province)
                                                        <option value="{{ $province->code }}"
                                                            {{ $konters->province_code == $province->code ? 'selected' : '' }}>
                                                            {{ $province->name }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                                @error('province_code')
                                                    <div class="invalid-feedback">
                                                        {{ $message }}
                                                    </div>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group row">
                                            <label class="col-4 col-form-label" for="regency_code">
                                                Kota
                                                <span class="text-danger">*</span>
                                            </label>
                                            <div class="col-8">
                                                <select name="regency_code" id="select_regencys"
                                                    class="form-control form-control-lg @error('regency_code') is-invalid @enderror"
                                                    value="{{ old('regency_code') }}">
                                                    <option value="{{ $konters->regency_code }}">
                                                        {{ $konters->regency->name }}</option>
                                                </select>
                                                @error('regency_code')
                                                    <div class="invalid-feedback">
                                                        {{ $message }}
                                                    </div>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6">
                                        <div class="form-group row">
                                            <label class="col-4 col-form-label" for="district_code">
                                                Kecamatan
                                                <span class="text-danger">*</span>
                                            </label>
                                            <div class="col-8">
                                                <select name="district_code" id="select_districts"
                                                    class="form-control form-control-lg @error('district_code') is-invalid @enderror"
                                                    value="{{ old('district_code') }}">
                                                    <option value="{{ $konters->district_code }}">
                                                        {{ $konters->district->name }}</option>
                                                </select>
                                                @error('district_code')
                                                    <div class="invalid-feedback">
                                                        {{ $message }}
                                                    </div>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group row">
                                            <label class="col-4 col-form-label" for="village_code">
                                                Kelurahan
                                                <span class="text-danger">*</span>
                                            </label>
                                            <div class="col-8">
                                                <select name="village_code" id="select_villages"
                                                    class="form-control form-control-lg @error('village_code') is-invalid @enderror"
                                                    value="{{ old('village_code') }}">
                                                    <option value="{{ $konters->village_code }}">
                                                        {{ $konters->village->name }}</option>
                                                </select>
                                                @error('village_code')
                                                    <div class="invalid-feedback">
                                                        {{ $message }}
                                                    </div>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6">
                                        <div class="form-group row">
                                            <label class="col-4 col-form-label" for="image">
                                                Image
                                                <span class="text-danger">*</span>
                                            </label>
                                            <div class="col-8">
                                                <div class="image-input image-input-outline" id="kt_image_1">
                                                    <div class="image-input-wrapper" @if (isset($konters->image)) style="background-image: url({{ asset('konterImg/' . $konters->image) }})" @else style="background-image: url({{ asset('back/media/users/blank.png') }})" @endif>
                                                    </div>
                                                    <label
                                                        class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow"
                                                        data-action="change" title="" data-original-title="Upload Image">
                                                        <i class="fa fa-pen icon-sm text-muted"></i>
                                                        <input type="file" class="@error('image') is-invalid @enderror"
                                                            value="{{ old('image') }}" name="image"
                                                            accept=".png, .jpg, .jpeg" onchange="previewFile(this);" />
                                                        <input type="hidden" name="profile_avatar_remove" />
                                                    </label>
                                                    <span
                                                        class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow"
                                                        data-action="cancel" title="Cancel image">
                                                        <i class="ki ki-bold-close icon-xs text-muted"></i>
                                                    </span>
                                                </div>
                                                <span class="form-text text-muted">Allowed file types: png, jpg,
                                                    jpeg.</span>
                                                <div wire:loading wire:target="image">
                                                    Uploading...
                                                </div>
                                                @error('image')
                                                    <div class="invalid-feedback">
                                                        {{ $message }}
                                                    </div>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group row">
                                            <label class="col-4 col-form-label" for="geojson">
                                                File GeoJSON
                                                <span class="text-danger">*</span>
                                            </label>
                                            <div class="col-8">
                                                <input
                                                    class="form-control form-control-lg @error('geojson') is-invalid @enderror"
                                                    value="{{ old('geojson') }}" name="geojson" type="file" />
                                                @error('geojson')
                                                    <div class="invalid-feedback">
                                                        {{ $message }}
                                                    </div>
                                                @enderror
                                                <br>
                                                File anda :
                                                <a href="{{ route('back-office.pos.download', $konters->id) }}">
                                                    {{ $konters->geojson }}
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <div class="form__actions">
                                    <button type="reset" onclick="history.back()" class="btn btn-secondary">Kembali</button>
                                    <button type="submit" class="btn btn-primary">Simpan</button>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
                <!--end::Card-->
            </div>
            <!--end::Container-->
        </div>
        <!--end::Entry-->
    </div>
@section('scripts')
    {{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script> --}}
    <script>
        $(document).ready(function() {
            $('#select_province').select2({
                placeholder: 'Pilih Provinsi',
            }).on('change', function() {
                var province_code = this.value;

                $("#select_regencys").html('');
                $.ajax({
                    url: "{{ url('back-office/konter/getRegencys') }}",
                    type: "POST",
                    data: {
                        province_code: province_code,
                        _token: '{{ csrf_token() }}'
                    },
                    dataType: 'json',
                    success: function(result) {
                        $("#select_regencys").empty();
                        $("<option value='-1'>- Kota / Kabupaten -</option>").appendTo(
                            "#select_regencys");
                        $.each(result.regencies, function(key, value) {
                            $("#select_regencys").append('<option value="' + value
                                .code +
                                '">' + value.name + '</option>');
                        });
                        $('#select_districts').html(
                            '<option value="">Pilih Kota Dulu</option>');
                    }
                });
            });
            $('#select_regencys').select2().on('change', function() {
                var regency_code = this.value;
                $("#select_districts").html('');
                $.ajax({
                    url: "{{ url('back-office/konter/getDistricts') }}",
                    type: "POST",
                    data: {
                        regency_code: regency_code,
                        _token: '{{ csrf_token() }}'
                    },
                    dataType: 'json',
                    success: function(result) {
                        $("#select_districts").empty();
                        $("<option value='-1'>- Kecamatan -</option>").appendTo(
                            "#select_districts");
                        $.each(result.districts, function(key, value) {
                            $("#select_districts").append('<option value="' + value
                                .code +
                                '">' + value.name + '</option>');
                        });
                        $('#select_villages').html(
                            '<option value="">Pilih Kecamatan Dulu</option>');
                    }
                });
            });
            $('#select_districts').select2({
                placeholder: 'Pilih Kecamatan',
            }).on('change', function() {
                var district_code = this.value;
                $("#select_villages").html('');
                $.ajax({
                    url: "{{ url('back-office/konter/getVillages') }}",
                    type: "POST",
                    data: {
                        district_code: district_code,
                        _token: '{{ csrf_token() }}'
                    },
                    dataType: 'json',
                    success: function(result) {
                        $("#select_villages").empty();
                        $("<option value='-1'>- Kelurahan -</option>").appendTo(
                            "#select_villages");
                        $.each(result.villages, function(key, value) {
                            $("#select_villages").append('<option value="' + value
                                .code +
                                '">' + value.name + '</option>');
                        });
                    }
                });
            });
            $('#select_villages').select2({
                placeholder: 'Pilih Kelurahan',
            });
        });
    </script>
@endsection
@endsection

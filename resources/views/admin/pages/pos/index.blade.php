<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-6 subheader-solid" id="kt_subheader">
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-1">
                <!--begin::Page Heading-->
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <!--begin::Page Title-->
                    <h5 class="text-dark font-weight-bold my-1 mr-5">Manage Content</h5>
                    <!--end::Page Title-->
                    <!--begin::Breadcrumb-->
                    <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">

                        <li class="breadcrumb-item">
                            <a href="" class="text-muted">Konter</a>
                        </li>
                    </ul>
                    <!--end::Breadcrumb-->
                </div>
                <!--end::Page Heading-->
            </div>
            <!--end::Info-->
        </div>
    </div>
    <!--end::Subheader-->
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <!--begin::Card-->
            <div class="card card-custom">
                <div class="card-header flex-wrap border-0 pt-6 pb-0">
                    <div class="card-title">
                        <h3 class="card-label">
                            Konter
                        </h3>
                    </div>
                    <div class="card-toolbar">
                        @can('Add Konter')
                            <a href="{{ route('back-office.pos.create') }}"
                                class="btn btn-primary font-weight-bolder btn-xs">
                                <i class="fas fa-plus-circle"></i>
                                Tambah
                            </a>
                        @endcan
                    </div>
                </div>

                <div class="card-body">
                    <div class="row d-flex justify-content-center">
                        <div class="col-3">
                            <input type="text" class="form-control form-control" placeholder="Cari kode pos.."
                                wire:model="search">
                        </div>
                        <div class="col-3">
                            <select type="text" class="form-control form-control" placeholder="Cari Kota / kabupaten.."
                                wire:model="codeProvince">
                                <option value="">- Provinsi -</option>
                                @foreach ($this->provinces as $item)
                                    <option value="{{ $item->code }}">{{ $item->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-2">
                            <select type="text" class="form-control form-control" placeholder="Cari Kota / kabupaten.."
                                wire:model="codeCity">
                                <option value="">- Kabupaten/Kota -</option>
                                @foreach ($this->regencies as $item)
                                    <option value="{{ $item->code }}">{{ $item->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-2">
                            <select type="text" class="form-control form-control" placeholder="Cari Kota / kabupaten.."
                                wire:model="codeDistrict">
                                <option value="">- Kecamatan -</option>
                                @foreach ($this->districts as $item)
                                    <option value="{{ $item->code }}">{{ $item->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-2">
                            <select type="text" class="form-control form-control" placeholder="Cari Kota / kabupaten.."
                                wire:model="codeVillage">
                                <option value="">- Kelurahan -</option>
                                @foreach ($this->villages as $item)
                                    <option value="{{ $item->code }}">{{ $item->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>Image</th>
                                <th>Kode Pos</th>
                                <th>Unduh GeoJSON</th>
                                <th class="text-center">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($this->konters as $item)
                                {{-- @if (Auth::user()->can('View Konter', 'Delete Konter', $item)) --}}
                                <tr>
                                    <td style="vertical-align: middle">
                                        <div class="symbol symbol-70">
                                            <div class="symbol-label"
                                                style="background-image: url({{ asset('konterImg/' . $item->image) }})">
                                            </div>
                                        </div>
                                    </td>
                                    <td style="vertical-align: middle">{{ $item->postalcode }}</td>
                                    <td style="vertical-align: middle">
                                        @if (isset($item->geojson))
                                            <button type="button" wire:click="download('{{ $item->id }}')"
                                                data-toggle="tooltip" title="Download" alt=" Download"
                                                class="btn btn-sm btn-success">
                                                Unduh
                                            </button>
                                        @else
                                            <button type="button" data-toggle="tooltip" title="Tidak ada geojson"
                                                alt=" Download" class="btn btn-sm btn-secondary">
                                                Unduh
                                            </button>
                                        @endif
                                    </td>
                                    <td class="text-center" style="vertical-align: middle">
                                        @can('Edit Konter')
                                            <a href="{{ route('back-office.pos.edit', $item->id) }}"
                                                data-toggle="tooltip" title="Edit" alt="Edit"
                                                class="btn btn-sm btn-warning btn-icon">
                                                <i class="far fa-edit"></i>
                                            </a>
                                        @endcan
                                        @can('Delete Konter')
                                            <button type="button" wire:click="deleteConfirm('{{ $item->id }}')"
                                                data-toggle="tooltip" title="Delete" alt=" Delete"
                                                class="btn btn-sm btn-danger btn-icon">
                                                <i class="far fa-trash-alt"></i>
                                            </button>
                                        @endcan
                                    </td>
                                </tr>
                                {{-- @endif --}}
                            @empty
                                <tr>
                                    <td colspan="5" class="text-center">
                                        Belum Ada Data
                                    </td>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                    <hr>
                    {{ $this->konters->links() }}
                </div>
            </div>
        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>

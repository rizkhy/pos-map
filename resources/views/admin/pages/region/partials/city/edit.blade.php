@extends('admin.layout.master')
@section('content')
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <!--begin::Subheader-->
        <div class="subheader py-2 py-lg-6 subheader-solid" id="kt_subheader">
            <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center flex-wrap mr-1">
                    <!--begin::Page Heading-->
                    <div class="d-flex align-items-baseline flex-wrap mr-5">
                        <!--begin::Page Title-->
                        <h5 class="text-dark font-weight-bold my-1 mr-5">Manage Content</h5>
                        <!--end::Page Title-->
                        <!--begin::Breadcrumb-->
                        <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">

                            <li class="breadcrumb-item">
                                <a href="" class="text-muted">Wilayah</a>
                            </li>
                        </ul>
                        <!--end::Breadcrumb-->
                    </div>
                    <!--end::Page Heading-->
                </div>
                <!--end::Info-->
            </div>
        </div>
        <!--end::Subheader-->
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class="container">
                <!--begin::Card-->
                <div class="card card-custom">
                    <div class="card-header flex-wrap border-0 pt-6 pb-0">
                        <div class="card-title">
                            <h3 class="card-label">
                                Tambah Wilayah
                            </h3>
                        </div>
                    </div>
                    <div class="card-body">

                        <form action="{{ route('back-office.region.update-regency', $regency->id) }}"
                            class="form" enctype="multipart/form-data" method="POST">
                            @csrf
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-6">
                                        <div class="form-group row">
                                            <label class="col-3 col-form-label" for="code" autocomplete="off">
                                                Kode
                                                <span class="text-danger">*</span>
                                            </label>
                                            <div class="col-9">
                                                <input type="text"
                                                    class="form-control form-control-lg @error('code') is-invalid @enderror"
                                                    name="code" value="{{ old('code', $regency->code) }}">
                                                @error('code')
                                                    <div class="invalid-feedback">
                                                        {{ $message }}
                                                    </div>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group row">
                                            <label class="col-3 col-form-label" for="name">
                                                Provinsi
                                                <span class="text-danger">*</span>
                                            </label>
                                            <div class="col-9">
                                                <select name="province_code" id="select_province"
                                                    class="form-control form-control-lg @error('province_code') is-invalid @enderror"
                                                    value="{{ old('province_code') }}">
                                                    <option value="">- Provinsi -</option>
                                                    @foreach ($provinces as $province)
                                                        <option value="{{ $province->code }}"
                                                            {{ $regency->province_code == $province->code ? 'selected' : '' }}>
                                                            {{ $province->name }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                                @error('province_code')
                                                    <div class="invalid-feedback">
                                                        {{ $message }}
                                                    </div>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6">
                                        <div class="form-group row">
                                            <label class="col-3 col-form-label" for="name">
                                                Kabupaten / Kota
                                                <span class="text-danger">*</span>
                                            </label>
                                            <div class="col-9">
                                                <input type="text"
                                                    class="form-control form-control-lg @error('name') is-invalid @enderror"
                                                    name="name" value="{{ old('name', $regency->name) }}">
                                                @error('name')
                                                    <div class="invalid-feedback">
                                                        {{ $message }}
                                                    </div>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group row">
                                            <label class="col-3 col-form-label" for="geojson">
                                                GeoJSON
                                            </label>
                                            <div class="col-9">
                                                <input type="file"
                                                    class="form-control form-control-lg @error('geojson') is-invalid @enderror"
                                                    name="geojson">
                                                @error('geojson')
                                                    <div class="invalid-feedback">
                                                        {{ $message }}
                                                    </div>
                                                @enderror
                                                <br>
                                                File anda :
                                                <a href="">
                                                    {{ $regency->geojson }}
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <div class="form__actions">
                                    <button type="reset" onclick="history.back()" class="btn btn-secondary">Kembali</button>
                                    <button type="submit" class="btn btn-primary">Simpan</button>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
                <!--end::Card-->
            </div>
            <!--end::Container-->
        </div>
        <!--end::Entry-->
    </div>
@endsection
@section('scripts')
    <script>
        $(document).ready(function() {
            $('#select_province').select2({
                placeholder: 'Pilih Provinsi',
            });
        });
    </script>
@endsection

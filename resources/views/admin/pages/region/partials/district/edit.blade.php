@extends('admin.layout.master')
@section('content')
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <!--begin::Subheader-->
        <div class="subheader py-2 py-lg-6 subheader-solid" id="kt_subheader">
            <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center flex-wrap mr-1">
                    <!--begin::Page Heading-->
                    <div class="d-flex align-items-baseline flex-wrap mr-5">
                        <!--begin::Page Title-->
                        <h5 class="text-dark font-weight-bold my-1 mr-5">Manage Content</h5>
                        <!--end::Page Title-->
                        <!--begin::Breadcrumb-->
                        <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">

                            <li class="breadcrumb-item">
                                <a href="" class="text-muted">Wilayah</a>
                            </li>
                        </ul>
                        <!--end::Breadcrumb-->
                    </div>
                    <!--end::Page Heading-->
                </div>
                <!--end::Info-->
            </div>
        </div>
        <!--end::Subheader-->
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class="container">
                <!--begin::Card-->
                <div class="card card-custom">
                    <div class="card-header flex-wrap border-0 pt-6 pb-0">
                        <div class="card-title">
                            <h3 class="card-label">
                                Tambah Wilayah
                            </h3>
                        </div>
                    </div>
                    <div class="card-body">

                        <form action="{{ route('back-office.region.update-district', $district->id) }}"
                            class="form" enctype="multipart/form-data" method="POST">
                            @csrf
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-6">
                                        <div class="form-group row">
                                            <label class="col-3 col-form-label" for="code">
                                                Kode
                                                <span class="text-danger">*</span>
                                            </label>
                                            <div class="col-9">
                                                <input type="text"
                                                    class="form-control form-control-lg @error('code') is-invalid @enderror"
                                                    name="code" value="{{ old('code', $district->code) }}"
                                                    autocomplete="off">
                                                @error('code')
                                                    <div class="invalid-feedback">
                                                        {{ $message }}
                                                    </div>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group row">
                                            <label class="col-3 col-form-label" for="name">
                                                Provinsi
                                                <span class="text-danger">*</span>
                                            </label>
                                            <div class="col-9">
                                                <select name="province_code" id="select_province"
                                                    class="form-control form-control-lg @error('province_code') is-invalid @enderror"
                                                    value="{{ old('province_code') }}">
                                                    <option value="">- Provinsi -</option>
                                                    @foreach ($provinces as $province)
                                                        <option value="{{ $province->code }}"
                                                            {{ $district->regency->province_code == $province->code ? 'selected' : '' }}>
                                                            {{ $province->name }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                                @error('province_code')
                                                    <div class="invalid-feedback">
                                                        {{ $message }}
                                                    </div>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6">
                                        <div class="form-group row">
                                            <label class="col-3 col-form-label" for="name">
                                                Kabupaten / Kota
                                                <span class="text-danger">*</span>
                                            </label>
                                            <div class="col-9">
                                                <select name="regency_code" id="select_regencys"
                                                    class="form-control form-control-lg @error('regency_code') is-invalid @enderror"
                                                    value="{{ old('regency_code') }}">
                                                    <option value="{{ $district->regency_code }}">
                                                        {{ $district->regency->name }}</option>
                                                </select>
                                                @error('regency_code')
                                                    <div class="invalid-feedback">
                                                        {{ $message }}
                                                    </div>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group row">
                                            <label class="col-3 col-form-label" for="name">
                                                Kecamatan
                                                <span class="text-danger">*</span>
                                            </label>
                                            <div class="col-9">
                                                <input type="text"
                                                    class="form-control form-control-lg @error('name') is-invalid @enderror"
                                                    name="name" value="{{ old('name', $district->name) }}"
                                                    autocomplete="off">
                                                @error('name')
                                                    <div class="invalid-feedback">
                                                        {{ $message }}
                                                    </div>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6">
                                        <div class="form-group row">
                                            <label class="col-3 col-form-label" for="geojson">
                                                GeoJSON
                                            </label>
                                            <div class="col-9">
                                                <input type="file"
                                                    class="form-control form-control-lg @error('geojson') is-invalid @enderror"
                                                    name="geojson">
                                                @error('geojson')
                                                    <div class="invalid-feedback">
                                                        {{ $message }}
                                                    </div>
                                                @enderror
                                                <br>
                                                File anda :
                                                <a href="">
                                                    {{ $district->geojson }}
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <div class="form__actions">
                                    <button type="reset" onclick="history.back()" class="btn btn-secondary">Kembali</button>
                                    <button type="submit" class="btn btn-primary">Simpan</button>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
                <!--end::Card-->
            </div>
            <!--end::Container-->
        </div>
        <!--end::Entry-->
    </div>
@endsection
@section('scripts')
    <script>
        $(document).ready(function() {
            $('#select_province').select2({
                placeholder: 'Pilih Provinsi',
            }).on('change', function() {
                var province_code = this.value;

                $("#select_regencys").html('');
                $.ajax({
                    url: "{{ url('back-office/konter/getRegencys') }}",
                    type: "POST",
                    data: {
                        province_code: province_code,
                        _token: '{{ csrf_token() }}'
                    },
                    dataType: 'json',
                    success: function(result) {
                        $("#select_regencys").empty();
                        $("<option value='-1'>- Kota / Kabupaten -</option>").appendTo(
                            "#select_regencys");
                        $.each(result.regencies, function(key, value) {
                            $("#select_regencys").append('<option value="' + value
                                .code +
                                '">' + value.name + '</option>');
                        });
                        $('#select_districts').html(
                            '<option value="">Pilih Kota Dulu</option>');
                    }
                });
            });

            $('#select_regencys').select2({
                placeholder: 'Pilih Kabupaten/Kota',
            });
        });
    </script>
@endsection

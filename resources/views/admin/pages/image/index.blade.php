<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-6 subheader-solid" id="kt_subheader">
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-1">
                <!--begin::Page Heading-->
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <!--begin::Page Title-->
                    <h5 class="text-dark font-weight-bold my-1 mr-5">Manage Content</h5>
                    <!--end::Page Title-->
                    <!--begin::Breadcrumb-->
                    <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">

                        <li class="breadcrumb-item">
                            <a href="" class="text-muted">Gambar</a>
                        </li>
                    </ul>
                    <!--end::Breadcrumb-->
                </div>
                <!--end::Page Heading-->
            </div>
            <!--end::Info-->
        </div>
    </div>
    <!--end::Subheader-->
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <!--begin::Card-->
            <div class="card card-custom">
                <div class="card-header flex-wrap border-0 pt-6 pb-0">
                    <div class="card-title">
                        <h3 class="card-label">
                            Gambar
                        </h3>
                    </div>
                    <div class="card-toolbar">
                        <a href="{{ route('back-office.image.create') }}"
                            class="btn btn-primary font-weight-bolder btn-xs">
                            <i class="fas fa-plus-circle"></i>
                            Tambah
                        </a>
                    </div>
                </div>

                <div class="card-body">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>Gambar Utama</th>
                                <th>Gambar</th>
                                <th>Nama Pos</th>
                                <th>Alamat</th>
                                <th class="text-center">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td style="vertical-align: middle">
                                    <div class="symbol symbol-70">
                                        <div class="symbol-label"
                                            style="background-image: url({{ asset('back/media/noimage.png') }})">
                                        </div>
                                    </div>
                                </td>
                                <td style="vertical-align: middle">
                                    <div class="symbol-group symbol-hover">
                                        <div class="symbol symbol-50 symbol-circle" data-toggle="tooltip" title=""
                                            data-original-title="Charlie Stone">
                                            <img alt="Pic"
                                                src="https://fastly.4sqi.net/img/general/600x600/44238927__20aYS_Ht8-tBEcJcNBdvbVp_jJYi7MSDOZ1a1-jVlg.jpg">
                                        </div>
                                        <div class="symbol symbol-50 symbol-circle" data-toggle="tooltip" title=""
                                            data-original-title="Luca Doncic">
                                            <img alt="Pic"
                                                src="https://lh5.googleusercontent.com/proxy/B8pG1WFXFU84uN8tSZt9uscM3BdzGff41l_0DcdTSLIku24xdKANVleEdZ-9DkFfai6oBXvr6NUDjPZ4-yq079PvckWmlMTpEhI4uvE53P6f1QG4vzbq63dAen3U8RE-Kehp5m9CIgeMc8E_poKXGSvFVU_bdbRnaLBxX_EhHQQG=w408-h306-k-no">
                                        </div>
                                        <div class="symbol symbol-50 symbol-circle" data-toggle="tooltip" title=""
                                            data-original-title="Nick Mana">
                                            <img alt="Pic"
                                                src="https://fastly.4sqi.net/img/general/600x600/9432070_MMgqxiNxvm8tr6kZbucSneYCiHJZk4c5rZgItM__mpQ.jpg">
                                        </div>
                                        <div class="symbol symbol-50 symbol-circle symbol-light">
                                            <span class="symbol-label font-weight-bold">5+</span>
                                        </div>
                                    </div>
                                </td>
                                <td style="vertical-align: middle">POS 110</td>
                                <td style="vertical-align: middle">Bandung</td>
                                <td class="text-center" style="vertical-align: middle">
                                    <a href="" data-toggle="tooltip" title="Edit" alt="Edit"
                                        class="btn btn-sm btn-warning btn-icon">
                                        <i class="far fa-edit"></i>
                                    </a>
                                    <button type="button" wire:click="removed()" data-toggle="tooltip" title="Delete"
                                        alt=" Delete" class="btn btn-sm btn-danger btn-icon">
                                        <i class="far fa-trash-alt"></i>
                                    </button>
                                </td>
                            </tr>
                            <tr>
                                <td style="vertical-align: middle">
                                    <div class="symbol symbol-70">
                                        <div class="symbol-label"
                                            style="background-image: url({{ asset('back/media/noimage.png') }})">
                                        </div>
                                    </div>
                                </td>
                                <td style="vertical-align: middle">
                                    <div class="symbol-group symbol-hover">
                                        <div class="symbol symbol-50 symbol-circle" data-toggle="tooltip" title=""
                                            data-original-title="Charlie Stone">
                                            <img alt="Pic"
                                                src="https://fastly.4sqi.net/img/general/600x600/44238927__20aYS_Ht8-tBEcJcNBdvbVp_jJYi7MSDOZ1a1-jVlg.jpg">
                                        </div>
                                        <div class="symbol symbol-50 symbol-circle" data-toggle="tooltip" title=""
                                            data-original-title="Luca Doncic">
                                            <img alt="Pic"
                                                src="https://lh5.googleusercontent.com/proxy/B8pG1WFXFU84uN8tSZt9uscM3BdzGff41l_0DcdTSLIku24xdKANVleEdZ-9DkFfai6oBXvr6NUDjPZ4-yq079PvckWmlMTpEhI4uvE53P6f1QG4vzbq63dAen3U8RE-Kehp5m9CIgeMc8E_poKXGSvFVU_bdbRnaLBxX_EhHQQG=w408-h306-k-no">
                                        </div>
                                        <div class="symbol symbol-50 symbol-circle" data-toggle="tooltip" title=""
                                            data-original-title="Nick Mana">
                                            <img alt="Pic"
                                                src="https://fastly.4sqi.net/img/general/600x600/9432070_MMgqxiNxvm8tr6kZbucSneYCiHJZk4c5rZgItM__mpQ.jpg">
                                        </div>
                                        <div class="symbol symbol-50 symbol-circle symbol-light">
                                            <span class="symbol-label font-weight-bold">5+</span>
                                        </div>
                                    </div>
                                </td>
                                <td style="vertical-align: middle">POS 190</td>
                                <td style="vertical-align: middle">Yogyakarta</td>
                                <td class="text-center" style="vertical-align: middle">
                                    <a href="" data-toggle="tooltip" title="Edit" alt="Edit"
                                        class="btn btn-sm btn-warning btn-icon">
                                        <i class="far fa-edit"></i>
                                    </a>
                                    <button type="button" wire:click="removed()" data-toggle="tooltip" title="Delete"
                                        alt=" Delete" class="btn btn-sm btn-danger btn-icon">
                                        <i class="far fa-trash-alt"></i>
                                    </button>
                                </td>
                            </tr>
                            {{-- @forelse($this->restorans as $restoran)
                                <tr>
                                    <td style="vertical-align: middle">{{ $restoran->title }}</td>
                                    <td style="vertical-align: middle">{{ $restoran->title }}</td>
                                    <td style="vertical-align: middle">{{ $restoran->title }}</td>
                                    <td style="vertical-align: middle">{{ $restoran->title }}</td>
                                    <td class="text-center" style="vertical-align: middle">
                                        <a href="{{ route('back-office.restoran.edit', $restoran->id) }}"
                                            data-toggle="tooltip" title="Edit" alt="Edit"
                                            class="btn btn-sm btn-warning btn-icon">
                                            <i class="far fa-edit"></i>
                                        </a>
                                        <button type="button" wire:click="removed('{{ $restoran->id }}')"
                                            data-toggle="tooltip" title="Delete" alt=" Delete"
                                            class="btn btn-sm btn-danger btn-icon">
                                            <i class="far fa-trash-alt"></i>
                                        </button>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="5" class="text-center">
                                        Belum Ada Data
                                    </td>
                                </tr>
                            @endforelse --}}
                        </tbody>
                    </table>
                    <hr>
                    {{-- {{ $this->restorans->links() }} --}}
                </div>
            </div>
        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>

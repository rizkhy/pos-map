<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-6 subheader-solid" id="kt_subheader">
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-1">
                <!--begin::Page Heading-->
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <!--begin::Page Title-->
                    <h5 class="text-dark font-weight-bold my-1 mr-5">Manage Content</h5>
                    <!--end::Page Title-->
                    <!--begin::Breadcrumb-->
                    <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">

                        <li class="breadcrumb-item">
                            <a href="" class="text-muted">Gambar</a>
                        </li>
                    </ul>
                    <!--end::Breadcrumb-->
                </div>
                <!--end::Page Heading-->
            </div>
            <!--end::Info-->
        </div>
    </div>
    <!--end::Subheader-->
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <!--begin::Card-->
            <div class="card card-custom">
                <div class="card-header flex-wrap border-0 pt-6 pb-0">
                    <div class="card-title">
                        <h3 class="card-label">
                            Tambah Gambar
                        </h3>
                    </div>
                </div>
                <div class="card-body">

                    <form wire:submit.prevent="saved" class="form" enctype="multipart/form-data">
                        @csrf
                        <div class="card-body">
                            <div class="form-group row">
                                <label class="col-2 col-form-label" for="konter.id">
                                    Pos
                                    <span class="text-danger">*</span>
                                </label>
                                <div class="col-9">
                                    <select
                                        class="form-control form-control-lg @error('konter.id') is-invalid @enderror"
                                        wire:model="konter.id" id="select_posname">
                                        <option value="">- Pilih -</option>
                                        @foreach ($this->konters as $item)
                                            <option value="{{ $item->id }}">{{ $item->name }}
                                            </option>
                                        @endforeach
                                    </select>
                                    @error('konter.id')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-2 col-form-label" for="konter.image">
                                    Gambar Utama
                                    <span class="text-danger">*</span>
                                </label>
                                <div class="col-9">
                                    <div class="image-input image-input-outline mr-5 mb-5" @if ($image == '') style="background-image: url({{ asset('back/media/noimage.png') }}); width: 168px; height: 168px" @endif>
                                        @if ($image)
                                            <div class="image-input-wrapper"
                                                style="background-image: url({{ $image ? $image->temporaryUrl() : '' }}); width: 168px; height: 168px; background-size: cover; background-position: center;">
                                            </div>
                                        @endif
                                        <label
                                            class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow"
                                            data-action="change" data-toggle="tooltip" title=""
                                            data-original-title="Upload Foto Produk">
                                            <i class="fa fa-pen icon-sm text-muted"></i>
                                            <input type="file" class="@error('konter.image') is-invalid @enderror"
                                                wire:model="konter.image" accept=".gif, .png, .bmp, .jpg, .jpeg" />
                                            <input type="hidden" name="photo_remove" />
                                        </label>
                                        <div wire:loading.block wire:target="konter.image"
                                            class="spinner spinner-primary spinner-lg"
                                            style="position: absolute; top: 50%; left: 30%;"></div>
                                        @error('konter.image')
                                            <span class="text-danger">
                                                {{ $message }}
                                            </span>
                                        @enderror
                                        <span
                                            class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow"
                                            data-action="cancel" data-toggle="tooltip" title="Hapus">
                                            <i class="ki ki-bold-close icon-xs text-muted"></i>
                                        </span>
                                    </div>

                                    <br>
                                    <span class="form-text text-muted">Allowed file types: png, jpg, jpeg.</span>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-2 col-form-label" for="images">
                                    Gambar
                                    <span class="text-danger">*</span>
                                </label>
                                <div class="col-9">
                                    @foreach ($otherimg as $item)
                                        <div class="image-input image-input-outline mr-5 mb-5" @if ($item['otherimg'] == '') style="background-image: url({{ asset('back/media/noimage.png') }}); width: 168px; height: 168px" @endif>
                                            <div class="image-input-wrapper"
                                                style="background-image: url({{ is_object($item['otherimg']) ? $item['otherimg']->temporaryUrl() : '' }}); width: 168px; height: 168px; background-size: cover; background-position: center;">
                                            </div>
                                            <label
                                                class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow"
                                                data-action="change" data-toggle="tooltip" title=""
                                                data-original-title="Upload Foto Produk">
                                                <i class="fa fa-pen icon-sm text-muted"></i>
                                                <input type="file"
                                                    class="@error('otherimg.{{ $loop->index }}.otherimg') is-invalid @enderror"
                                                    wire:model="otherimg.{{ $loop->index }}.otherimg"
                                                    accept=".gif, .png, .bmp, .jpg, .jpeg" />
                                                <input type="hidden" name="photo_remove" />
                                            </label>
                                            <div wire:loading.block
                                                wire:target="otherimg.{{ $loop->index }}.otherimg"
                                                class="spinner spinner-primary spinner-lg"
                                                style="position: absolute; top: 50%; left: 30%;">
                                            </div>
                                            @error('otherimg.{{ $loop->index }}.otherimg')
                                                <span class="text-danger">
                                                    {{ $message }}
                                                </span>
                                            @enderror
                                            <span
                                                class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow"
                                                data-action="cancel" data-toggle="tooltip" title="Hapus">
                                                <i class="ki ki-bold-close icon-xs text-muted"></i>
                                            </span>
                                        </div>
                                    @endforeach
                                    <br>
                                    <span class="form-text text-muted">Allowed file types: png, jpg, jpeg.</span>
                                    <button type="button" wire:click="add()" class="btn btn-light-success mt-2">Add
                                        Image</button>
                                </div>
                            </div>

                        </div>
                </div>
                <div class="card-footer">
                    <div class="form__actions">
                        <button type="reset" onclick="history.back()" class="btn btn-secondary">Kembali</button>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </div>
                </form>

            </div>
        </div>
        <!--end::Card-->
    </div>
    <!--end::Container-->
</div>
<!--end::Entry-->
</div>
@section('scripts')
    <script>
        $(document).ready(function() {
            $('#select_posname').select2({
                    placeholder: "Pos",
                    cache: true
                })
                .on('select2:select', function(e) {
                    var data = e.params.data;
                    @this.set('konter.id', data.id);
                });

        });

        window.addEventListener('render', event => {
            $('#select_posname').select2({
                placeholder: "Pos"
            });
        })
    </script>
@endsection

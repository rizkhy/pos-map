@extends('front.layout.master')
@section('content')
    <div style="height: 100%">
        <div class="pl-10 pr-10 pt-5" style="background-color: #0075A9; height: 12%">
            <div class="row">
                <div class="col-3">
                    <img src="{{ asset('back/media/logos/logo-kominfo-white.png') }}" alt="" width="300">
                </div>
            </div>

        </div>
        <div id="mapid"></div>
        <div class="d-block"
            style="background-color: #0075A9; height: auto; min-height: 20%; z-index: 10; width: 100%; ">
            <div class="pl-5 pt-5 pr-5 pb-0">
                <div class="row">
                    <div class="col-3">
                        <select name="province_code" id="select_province" class="form-control form-control-lg ">
                            <option value="">- Provinsi -</option>
                            @foreach ($provinces as $province)
                                <option value="{{ $province->code }}">{{ $province->name }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-3">
                        <select id="select_regencys" class="form-control  form-control-lg">
                            <option value="">- Kota / Kabupaten -</option>
                        </select>
                    </div>
                    <div class="col-3">
                        <select id="select_districts" class="form-control  form-control-lg">
                            <option value="">- Kecamatan -</option>
                        </select>
                    </div>
                    <div class="col-3">
                        <select id="select_villages" class="form-control  form-control-lg">
                            <option value="">- Kelurahan -</option>
                        </select>
                    </div>
                </div>
                <div class="row align-items-center mt-2">
                    <div class="col-6">
                        <div class="input-group mb-3">
                            <input type="text" name="search" id="search_konter" class="form-control form-control-lg"
                                placeholder="Masukkan Kode Pos.." autocomplete="off">
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="row justify-content-between align-items-center">
                            <div class="col-6">
                                <div id="poss" class="row">
                                </div>
                                <h3 class="font-weight-bolder text-white text-center">
                                    <span id="kodepos"></span>
                                </h3>
                            </div>
                            <div class="col-6 text-center">
                                <button value="" type="button" id="downloadImg"
                                    class="btn btn-primary btn-lg btn-block font-weight-bolder">
                                    Unduh Gambar
                                    <i class="flaticon-download ml-2"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-5" id="list_postalcode">
                    <div class="col-12">
                        <div class="row justify-content-center text-center text-white font-weight-boldest">

                            <div class="col-1">
                                No.
                            </div>
                            <div class="col-4">
                                Kode Pos
                            </div>
                            <div class="col-4">
                                Kelurahan
                            </div>
                            <div class="col-3">
                                Unduh
                            </div>

                        </div>
                        <hr color="white">
                    </div>
                </div>
                <div class="row mt-5" id="list_search_postalcode">
                    <div class="col-12">
                        <div class="row justify-content-center text-center text-white font-weight-boldest">

                            <div class="col-2">
                                Provinsi
                            </div>
                            <div class="col-3">
                                Kab/kota
                            </div>
                            <div class="col-3">
                                Kecamatan
                            </div>
                            <div class="col-3">
                                Kelurahan
                            </div>
                            <div class="col-1">
                                Unduh
                            </div>

                        </div>
                        <hr color="white">
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('styles')
    <link rel="stylesheet" href="{{ asset('front/dist/leaflet.css') }}" />
    <link rel="stylesheet" href="{{ asset('front/dist/MarkerCluster.css') }}" />
    <link rel="stylesheet" href="{{ asset('front/dist/MarkerCluster.Default.css') }}" />


    <style>
        /* width */
        ::-webkit-scrollbar {
            width: 6px;
        }

        /* Track */
        ::-webkit-scrollbar-track {
            background: #f1f1f1;
        }

        /* Handle */
        ::-webkit-scrollbar-thumb {
            background: #0075A9;
        }

        /* Handle on hover */
        ::-webkit-scrollbar-thumb:hover {
            background: #015477;
        }

        #mapid {
            /* display: block; */
            height: 68%;
        }

    </style>
@endsection

@section('scripts')
    <script src="{{ asset('front/dist/leaflet.js') }}"></script>
    <script type="text/javascript" src="{{ asset('front/dist/leaflet.ajax.min.js') }}">
    </script>

    <script>
        var map = L.map('mapid', {
            zoomControl: false
        }).setView([-3.5754843227739475, 118.78908127999408], 5);

        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png').addTo(map);

        L.control.zoom({
            position: 'bottomright'
        }).addTo(map);

        var polygon = new L.LayerGroup();
        polygon.addTo(map);
    </script>
    <script>
        $(document).ready(function() {
            $('#list_search_postalcode').hide();
            $('#list_postalcode').hide();
            $('#downloadImg').hide();
            $('#downloadImgVill').hide();

            $('#select_province').select2({
                placeholder: 'Pilih Provinsi',
            }).on('change', function() {
                var province_code = this.value;
                $("#select_regencys").html('');
                $('#list_search_postalcode').hide();
                $('#list_search_postalcode .val-postalcode').empty();
                $('#list_postalcode').hide();
                $('#list_postalcode .val-postalcode').empty();
                $.ajax({
                    url: "{{ url('/getRegencys') }}",
                    type: "POST",
                    data: {
                        province_code: province_code,
                        _token: '{{ csrf_token() }}'
                    },
                    dataType: 'json',
                    success: function(result) {
                        $('#list_postalcode').hide();
                        $('#list_postalcode .val-postalcode').empty();
                        $('#list_search_postalcode').hide();
                        $('#list_search_postalcode .val-postalcode').empty();
                        $("#kodepos").empty();
                        $('#downloadImg').hide();
                        $("#select_regencys").empty();
                        $("<option value='-1'>- Kota / Kabupaten -</option>").appendTo(
                            "#select_regencys");
                        $.each(result.regencies, function(key, value) {
                            $("#select_regencys").append('<option value="' + value
                                .code +
                                '">' + value.name + '</option>');
                        });
                    }
                });
                $.ajax({
                    url: "{{ url('/getProvGeoJSON') }}",
                    type: "POST",
                    data: {
                        code: province_code,
                        _token: '{{ csrf_token() }}'
                    },
                    dataType: 'json',
                    success: function(result) {
                        $.each(result.provinces, function(key, value) {
                            polygon.clearLayers();
                            var geojsonLayer = new L.GeoJSON.AJAX(value
                                .geojson).on('data:loaded',
                                function() {
                                    map.fitBounds(geojsonLayer.getBounds())
                                });
                            polygon.addLayer(geojsonLayer);
                        });
                    }
                });
            });

            $('#select_regencys').select2({
                placeholder: 'Pilih Kabupaten/Kota',
            }).on('change', function() {
                var regency_code = this.value;
                $("#select_districts").html('');
                $('#list_search_postalcode').hide();
                $('#list_search_postalcode .val-postalcode').empty();
                $.ajax({
                    url: "{{ url('/getDistricts') }}",
                    type: "POST",
                    data: {
                        regency_code: regency_code,
                        _token: '{{ csrf_token() }}'
                    },
                    dataType: 'json',
                    success: function(result) {
                        $("#kodepos").empty();
                        $('#downloadImg').hide();
                        $("#select_districts").empty();
                        $("<option value='-1'>- Kecamatan -</option>").appendTo(
                            "#select_districts");
                        $.each(result.districts, function(key, value) {
                            $("#select_districts").append('<option value="' + value
                                .code +
                                '">' + value.name + '</option>');
                        });
                    }
                });
                $.ajax({
                    url: "{{ url('/getRegGeoJSON') }}",
                    type: "POST",
                    data: {
                        code: regency_code,
                        _token: '{{ csrf_token() }}'
                    },
                    dataType: 'json',
                    success: function(result) {
                        $.each(result.provinces, function(key, value) {
                            polygon.clearLayers();
                            var geojsonLayer = new L.GeoJSON.AJAX(value
                                .geojson).on('data:loaded',
                                function() {
                                    map.fitBounds(geojsonLayer.getBounds())
                                });
                            polygon.addLayer(geojsonLayer);
                        });
                    }
                });
            });

            $('#select_districts').select2({
                placeholder: 'Pilih Kecamatan',
            }).on('change', function() {
                var district_code = this.value;
                $("#select_villages").html('');
                $('#list_search_postalcode').hide();
                $('#list_search_postalcode .val-postalcode').empty();
                $.ajax({
                    url: "{{ url('/getVillages') }}",
                    type: "POST",
                    data: {
                        district_code: district_code,
                        _token: '{{ csrf_token() }}'
                    },
                    dataType: 'json',
                    success: function(result) {
                        $("#kodepos").empty();
                        $('#downloadImg').hide();
                        $("#select_villages").empty();
                        $("<option value='-1'>- Kelurahan -</option>").appendTo(
                            "#select_villages");
                        $.each(result.villages, function(key, value) {
                            $("#select_villages").append('<option value="' + value
                                .code +
                                '">' + value.name + '</option>');
                        });
                    }
                });
                $.ajax({
                    url: "{{ url('/getDisGeoJSON') }}",
                    type: "POST",
                    data: {
                        code: district_code,
                        _token: '{{ csrf_token() }}'
                    },
                    dataType: 'json',
                    success: function(result) {
                        $.each(result.provinces, function(key, value) {
                            polygon.clearLayers();
                            var geojsonLayer = new L.GeoJSON.AJAX(value
                                .geojson).on('data:loaded',
                                function() {
                                    map.fitBounds(geojsonLayer.getBounds())
                                });
                            polygon.addLayer(geojsonLayer);
                        });
                    }
                });
            });

            $('#select_villages').select2({
                placeholder: 'Pilih Kelurahan',
            }).on('change', function() {
                var village_code = this.value;
                $("#select_postalcode").html('');
                $('#list_search_postalcode').hide();
                $('#list_search_postalcode .val-postalcode').empty();
                $('#list_postalcode').hide();
                $('#list_postalcode .val-postalcode').empty();
                $.ajax({
                    url: "{{ url('/getPosCodeVill') }}",
                    type: "POST",
                    data: {
                        village_code: village_code,
                        _token: '{{ csrf_token() }}'
                    },
                    dataType: 'json',
                    success: function(result) {
                        if (result.konters.length == 0) {
                            $('#list_postalcode').hide();
                            $('#list_postalcode .val-postalcode').empty();
                        }
                        if (result.konters.length > 0) {
                            $.each(result.konters, function(key, value) {
                                $('#list_postalcode').show();

                                if (value.category == "super") {
                                    var icon = "fas fa-star";
                                } else if (value.category == "mandiri") {
                                    var icon = "fas fa-building";
                                } else {
                                    var icon = "";
                                }

                                if (!!value.image) {
                                    var downloadImage = `
                                            <a href="{{ url('/download-image/` + value.id + `') }}" class="btn btn-light-primary btn-sm btn-block font-weight-bolder">
                                                <i class="flaticon-download"></i>
                                            </a>
                                    `;
                                } else {
                                    var downloadImage = "Tidak ada gambar";
                                }

                                var htmlValue = `
                                <div class="col-12 val-postalcode">
                                    <div class="row justify-content-center text-center text-white align-items-center font-weight-bolder">

                                        <div class="col-1">
                                            ` + (key + 1) + `
                                        </div>
                                        <div class="col-4">
                                            ` + value.postalcode + `
                                            <i class="` + icon + ` text-white ml-3"></i>
                                        </div>
                                        <div class="col-4">
                                            ` + value.village.name + `
                                        </div>
                                        <div class="col-3">
                                            ` + downloadImage + `
                                        </div>

                                    </div>
                                    <hr color="white">
                                </div>
                            `;
                                $('#list_postalcode').append(htmlValue);

                            });
                        }
                    }
                });
                $.ajax({
                    url: "{{ url('/getVilGeoJSON') }}",
                    type: "POST",
                    data: {
                        code: village_code,
                        _token: '{{ csrf_token() }}'
                    },
                    dataType: 'json',
                    success: function(result) {
                        $.each(result.provinces, function(key, value) {
                            polygon.clearLayers();
                            var geojsonLayer = new L.GeoJSON.AJAX(value
                                .geojson).on('data:loaded',
                                function() {
                                    map.fitBounds(geojsonLayer.getBounds())
                                });
                            polygon.addLayer(geojsonLayer);
                        });
                    }
                });
            });

            $("#select_postalcode").select2({
                placeholder: 'Pilih Kode Pos',
            }).on('change', function() {
                var postalcode = this.value;
                var id = $(this).data('id');
                $.ajax({
                    url: "{{ url('/getUrlDownload') }}",
                    type: "POST",
                    data: {
                        postalcode: postalcode,
                        _token: '{{ csrf_token() }}'
                    },
                    dataType: 'json',
                    success: function(result) {
                        if (result.length == 0) {
                            $("#kodepos").empty();
                            $('#downloadImgVill').hide();
                            $('#downloadImgVill').val(empty);
                        } else {
                            $.each(result.konters, function(key, value) {
                                $("#kodepos").empty();
                                $('#downloadImgVill').show();
                                $('#downloadImgVill').val(value.id);
                            });
                        }
                    }
                });
            });

            $('#search_konter').on('keyup', function() {
                var search = this.value;
                var id = $(this).data('id');
                $("#select_province").empty();
                $("#select_regencys").empty();
                $("#select_districts").empty();
                $("#select_villages").empty();
                $('#list_search_postalcode').hide();
                $('#list_search_postalcode .val-postalcode').empty();
                $('#list_postalcode').hide();
                $('#list_postalcode .val-postalcode').empty();
                $.ajax({
                    url: "{{ url('/getKonterGeoJSON') }}",
                    type: "POST",
                    data: {
                        search: search,
                        _token: '{{ csrf_token() }}'
                    },
                    dataType: 'json',
                    success: function(result) {
                        $.each(result, function(key, value) {
                            polygon.clearLayers();
                            var geojsonLayer = new L.GeoJSON.AJAX(value
                                .geojson).on('data:loaded',
                                function() {
                                    map.fitBounds(geojsonLayer.getBounds())
                                });
                            polygon.addLayer(geojsonLayer);
                        });
                        if (result.length == 0) {
                            $('#list_search_postalcode').hide();
                            $('#list_search_postalcode .val-postalcode').empty();
                            $('#list_postalcode').hide();
                            $('#list_postalcode .val-postalcode').empty();
                        }
                        if (result.length > 0) {
                            $.each(result, function(key, value) {
                                $('#list_search_postalcode').show();

                                if (value.category == "super") {
                                    var icon = "fas fa-star";
                                } else if (value.category == "mandiri") {
                                    var icon = "fas fa-building";
                                } else {
                                    var icon = "";
                                }

                                if (!!value.image) {
                                    var downloadImage = `
                                            <a href="{{ url('/download-image/` + value.id + `') }}" class="btn btn-light-primary btn-sm btn-block font-weight-bolder">
                                                <i class="flaticon-download"></i>
                                            </a>
                                    `;
                                } else {
                                    var downloadImage = "Tidak ada gambar";
                                }

                                var htmlValue = `
                                <div class="col-12 val-postalcode">
                                    <div class="row justify-content-center text-center text-white align-items-center font-weight-bolder">

                                        <div class="col-2">
                                            ` + value.provinces.name + `
                                            <i class="` + icon + ` text-white ml-3"></i>
                                        </div>
                                        <div class="col-3">
                                            ` + value.regency.name + `
                                        </div>
                                        <div class="col-3">
                                            ` + value.district.name + `
                                        </div>
                                        <div class="col-3">
                                            ` + value.village.name + `
                                        </div>
                                        <div class="col-1">
                                            ` + downloadImage + `
                                        </div>

                                    </div>
                                    <hr color="white">
                                </div>
                                `;
                                $('#list_search_postalcode').append(htmlValue);

                            });
                        }
                    }
                });

            });

            $('.downloadImg').on('click', function() {
                var id = this.value;
                $.ajax({
                    type: "POST",
                    url: "{{ url('/download') }}",
                    data: {
                        id: id,
                        _token: '{{ csrf_token() }}'
                    },
                    xhrFields: {
                        responseType: 'blob'
                    },
                    success: function(blob, status, xhr) {
                        var filename = "";
                        var disposition = xhr.getResponseHeader('Content-Disposition');
                        if (disposition && disposition.indexOf('attachment') !== -1) {
                            var filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
                            var matches = filenameRegex.exec(disposition);
                            if (matches != null && matches[1]) filename = matches[1].replace(
                                /['"]/g, '');
                        }

                        if (typeof window.navigator.msSaveBlob !== 'undefined') {
                            window.navigator.msSaveBlob(blob, filename);
                        } else {
                            var URL = window.URL || window.webkitURL;
                            var downloadUrl = URL.createObjectURL(blob);

                            if (filename) {
                                var a = document.createElement("a");
                                if (typeof a.download === 'undefined') {
                                    window.location.href = downloadUrl;
                                } else {
                                    a.href = downloadUrl;
                                    a.download = filename;
                                    document.body.appendChild(a);
                                    a.click();
                                }
                            } else {
                                window.location.href = downloadUrl;
                            }

                            setTimeout(function() {
                                URL.revokeObjectURL(downloadUrl);
                            }, 100);
                        }
                    }
                });
            });

            $('#downloadImgVill').on('click', function() {
                var id = this.value;
                $.ajax({
                    type: "POST",
                    url: "{{ url('/downloadVill') }}",
                    data: {
                        id: id,
                        _token: '{{ csrf_token() }}'
                    },
                    xhrFields: {
                        responseType: 'blob'
                    },
                    success: function(blob, status, xhr) {
                        var filename = "";
                        var disposition = xhr.getResponseHeader('Content-Disposition');
                        if (disposition && disposition.indexOf('attachment') !== -1) {
                            var filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
                            var matches = filenameRegex.exec(disposition);
                            if (matches != null && matches[1]) filename = matches[1].replace(
                                /['"]/g, '');
                        }

                        if (typeof window.navigator.msSaveBlob !== 'undefined') {
                            window.navigator.msSaveBlob(blob, filename);
                        } else {
                            var URL = window.URL || window.webkitURL;
                            var downloadUrl = URL.createObjectURL(blob);

                            if (filename) {
                                var a = document.createElement("a");
                                if (typeof a.download === 'undefined') {
                                    window.location.href = downloadUrl;
                                } else {
                                    a.href = downloadUrl;
                                    a.download = filename;
                                    document.body.appendChild(a);
                                    a.click();
                                }
                            } else {
                                window.location.href = downloadUrl;
                            }

                            setTimeout(function() {
                                URL.revokeObjectURL(downloadUrl);
                            }, 100);
                        }
                    }
                });
            });

        });
    </script>

@endsection

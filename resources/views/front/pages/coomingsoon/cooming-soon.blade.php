<div class="pl-10 pr-10 pt-5" style="background-color: #0075A9; height: 100%">
    <div class="row mb-5">
        <div class="col-3">
            <img src="{{ asset('back/media/logos/logo-kominfo-white.png') }}" alt="" width="300">
        </div>
    </div>

    <div class="row justify-content-center align-content-center text-center mt-30">
        <div class="col-12">
            <span class="text-white" style="font-size: 50px">Coming Soon</span>
            <br><br>
            <a href="{{ route('landing-page') }}" class="btn btn-white mt-5">
                View Maps
            </a>
        </div>
    </div>

</div>

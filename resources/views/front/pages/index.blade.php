<div style="position: relative">
    <div class="p-10" style="background-color: #FF6F00; height: 20vh">
        <div class="row">
            <div class="col-1">
                <img src="{{ asset('back/media/logos/pos-white.png') }}" alt="" width="130">
            </div>
            <div class="col-2 line-height-md ml-17 mt-3 text-white">
                <span style="font-size: 24px; font-weight: bolder">POS POINT </span>
                <br>
                <span style="font-size: 12px; padding-top: -30px">
                    by Pos Indonesia
                </span>
            </div>
            <div class="col-2 mt-3">
                <select wire:model="province_code" id="select_province" class="form-control select2 form-control-lg">
                    <option value="">- Provinsi -</option>
                    @foreach ($this->provinces as $province)
                        <option value="{{ $province->code }}">{{ $province->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-2 mt-3">
                <select wire:model="regency_code" id="select_regency" class="form-control select2 form-control-lg">
                    <option value="">- Kota / Kabupaten -</option>
                    @foreach ($this->regencies as $regency)
                        <option value="{{ $regency->code }}">{{ $regency->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-2 mt-3">
                <select wire:model="district_code" id="select_district" class="form-control select2 form-control-lg">
                    <option value="">- Kecamatan -</option>
                    @foreach ($this->districts as $district)
                        <option value="{{ $district->code }}">{{ $district->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-2 mt-3">
                <select wire:model="village_code" id="select_village" class="form-control select2 form-control-lg">
                    <option value="">- Kelurahan -</option>
                    @foreach ($this->villages as $village)
                        <option value="{{ $village->code }}">{{ $village->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
    {{-- <button class="btn btn-white d-none" style="position: absolute; top: 50%; left: 29.9%; height: 70px; z-index: 20;">
        <b>
            &lt;
        </b>
    </button>
    <div class="d-none"
        style="position: absolute; background-color: #FFF; height: 100vh; z-index: 10; width: 30%; margin-top: -20vh; box-shadow: 5px 0px 40px rgba(56, 56, 56, 0.5); overflow-y: scroll;">

        <img src="https://lh5.googleusercontent.com/proxy/B8pG1WFXFU84uN8tSZt9uscM3BdzGff41l_0DcdTSLIku24xdKANVleEdZ-9DkFfai6oBXvr6NUDjPZ4-yq079PvckWmlMTpEhI4uvE53P6f1QG4vzbq63dAen3U8RE-Kehp5m9CIgeMc8E_poKXGSvFVU_bdbRnaLBxX_EhHQQG=w408-h306-k-no"
            class="mb-5" width="100%" alt="">

        <div class="p-5">
            <h5 class="font-weight-bolder">
                Kantor Pos Yogyakarta
            </h5>
            <span class="text-muted">
                Papua
            </span>

            <hr class="mb-5 mt-5">

            <div class="row mb-3">
                <div class="col-1">
                    <i class="flaticon2-location text-warning"></i>
                </div>
                <div class="col-10">
                    Jl. Ngapak - Kentheng, Modinan, Banyuraden, Kec. Gamping, Kabupaten Sleman, Daerah Istimewa
                    Yogyakarta
                </div>
            </div>

            <div class="row mb-3">
                <div class="col-1">
                    <i class="flaticon2-crisp-icons-1 text-warning"></i>
                </div>
                <div class="col-10">
                    Kode Pos 20999
                </div>
            </div>

            <div class="row mb-3">
                <div class="col-1">
                    <i class="flaticon2-paperplane text-warning"></i>
                </div>
                <div class="col-10">
                    wa.me/6281908765432
                </div>
            </div>

            <div class="row">
                <div class="col-1">
                    <i class="flaticon2-phone text-warning"></i>
                </div>
                <div class="col-10">
                    (0951)321908
                </div>
            </div>

            <hr class="mb-5 mt-5">

            <h5 class="font-weight-bolder mb-5">
                Photo
            </h5>

            <div class="row">
                <div class="col-6">
                    <img class="card-img mb-5 rounded-lg"
                        src="https://kotaindustri.com/wp-content/uploads/2021/06/FOTO-HAL-09-KANTOR-POS-WILAYAH-KUPANG-EDISI-160919-740x431@2x.jpeg">

                    <img class="card-img mb-5 rounded-lg"
                        src="https://fastly.4sqi.net/img/general/600x600/44238927__20aYS_Ht8-tBEcJcNBdvbVp_jJYi7MSDOZ1a1-jVlg.jpg">
                </div>
                <div class="col-6">
                    <img class="card-img mb-5 rounded-lg"
                        src="https://lh5.googleusercontent.com/proxy/B8pG1WFXFU84uN8tSZt9uscM3BdzGff41l_0DcdTSLIku24xdKANVleEdZ-9DkFfai6oBXvr6NUDjPZ4-yq079PvckWmlMTpEhI4uvE53P6f1QG4vzbq63dAen3U8RE-Kehp5m9CIgeMc8E_poKXGSvFVU_bdbRnaLBxX_EhHQQG=w408-h306-k-no">

                    <img class="card-img mb-5 rounded-lg"
                        src="https://fastly.4sqi.net/img/general/600x600/9432070_MMgqxiNxvm8tr6kZbucSneYCiHJZk4c5rZgItM__mpQ.jpg">
                </div>
            </div>
        </div>
    </div> --}}
    {{-- <div class="row" style="position: absolute; top: 150px; z-index: 999">
        <div class="col-1">
            <img src="{{ asset('back/media/logos/pos-white.png') }}" alt="" width="130">
        </div>
        <div class="col-2 line-height-md ml-17 mt-3 text-white">
            <span style="font-size: 24px; font-weight: bolder">POS POINT </span>
            <br>
            <span style="font-size: 12px; padding-top: -30px">
                by Pos Indonesia
            </span>
        </div>
        <div class="col-2 mt-3">
            <select name="" id="" class="form-control form-control-lg">
                <option value="">- Provinsi -</option>
            </select>
        </div>
        <div class="col-2 mt-3">
            <select name="" id="" class="form-control form-control-lg">
                <option value="">- Kota / Kabupaten -</option>
            </select>
        </div>
        <div class="col-2 mt-3">
            <select name="" id="" class="form-control form-control-lg">
                <option value="">- Kecamatan -</option>
            </select>
        </div>
        <div class="col-2 mt-3">
            <select name="" id="" class="form-control form-control-lg">
                <option value="">- Kelurahan -</option>
            </select>
        </div>
    </div> --}}
    <div id="mapid"></div>
</div>
@section('styles')
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.1/dist/leaflet.css"
        integrity="sha512-Rksm5RenBEKSKFjgI3a41vrjkw4EVPlJ3+OiI65vTjIdo9brlAacEuKOiQ5OFh7cOI1bkDwLqdLw3Zg0cRJAAQ=="
        crossorigin="" />
    <link rel="stylesheet" href="https://unpkg.com/leaflet.markercluster@1.4.1/dist/MarkerCluster.css" />
    <link rel="stylesheet" href="https://unpkg.com/leaflet.markercluster@1.4.1/dist/MarkerCluster.Default.css" />


    <style>
        /* width */
        ::-webkit-scrollbar {
            width: 6px;
        }

        /* Track */
        ::-webkit-scrollbar-track {
            background: #f1f1f1;
        }

        /* Handle */
        ::-webkit-scrollbar-thumb {
            background: #FF6F00;
        }

        /* Handle on hover */
        ::-webkit-scrollbar-thumb:hover {
            background: #e26302;
        }

        #mapid {
            height: 80vh;
        }

    </style>
@endsection

@section('scripts')
    <!-- Leaflet JavaScript -->
    <!-- Make sure you put this AFTER Leaflet's CSS -->
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css"
        integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A=="
        crossorigin="" />
    <link rel="stylesheet" href="https://unpkg.com/leaflet.markercluster@1.4.1/dist/MarkerCluster.css" />
    <link rel="stylesheet" href="https://unpkg.com/leaflet.markercluster@1.4.1/dist/MarkerCluster.Default.css" />

    <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js"
        integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA=="
        crossorigin=""></script>
    <script src="https://unpkg.com/leaflet.markercluster@1.4.1/dist/leaflet.markercluster.js"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>

    <script>
        var posIcon = L.icon({
            iconUrl: 'front/images/marker.png',
            iconSize: [30, 30],
            iconAnchor: [15, 30],
            popupAnchor: [-2, -30]
        });

        var map = L.map('mapid', {
            zoomControl: false
        }).setView([-0.789275, 113.921327], 5);

        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png').addTo(map);

        var markers = L.markerClusterGroup();

        axios.get('{{ route('api.konter.index') }}')
            .then(function(response) {
                var marker = L.geoJSON(response.data, {
                    pointToLayer: function(geoJsonPoint, latlng) {
                        return L.marker(latlng, {
                            icon: posIcon
                        }).bindPopup(function(layer) {
                            return layer.feature.properties.map_popup_content;
                        });
                    }
                });
                markers.addLayer(marker);
            })
            .catch(function(error) {
                console.log(error);
            });

        map.addLayer(markers);

        // L.marker([-7.7832589, 110.4142924], {
        //     icon: posIcon
        // }).bindPopup("<b>Hello world!</b><br>I am a popup.").addTo(map);

        L.control.zoom({
            position: 'bottomright'
        }).addTo(map);
    </script>

    <script>
        $(document).ready(function() {
            $('#select_province').select2({
                    placeholder: "Provinsi",
                    cache: true
                })
                .on('select2:select', function(e) {
                    var data = e.params.data;
                    @this.set('province_code', data.id);
                });

            $('#select_regency').select2({
                    placeholder: "Kabupaten/Kota",
                    cache: true
                })
                .on('select2:select', function(e) {
                    var data = e.params.data;
                    @this.set('regency_code', data.id);
                });

            $('#select_district').select2({
                    placeholder: "Kecamatan",
                    cache: true
                })
                .on('select2:select', function(e) {
                    let data = e.params.data;
                    if (data.selected) {
                        @this.set('district_code', data.id);
                    }
                });

            $('#select_village').select2({
                    placeholder: "Kelurahan",
                    cache: true
                })
                .on('select2:select', function(e) {
                    let data = e.params.data;
                    if (data.selected) {
                        @this.set('village_code', data.id);
                    }
                });
        });

        window.addEventListener('render', event => {
            $('#select_province').select2({
                placeholder: "Provinsi"
            });
            $('#select_regency').select2({
                placeholder: "Kabupaten/Kota"
            });
            $('#select_district').select2({
                placeholder: "Kecamatan"
            });
            $('#select_village').select2({
                placeholder: "Kelurahan"
            });
        })
    </script>
@endsection

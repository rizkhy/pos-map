<?php

namespace App\Http\Controllers\Konter;

use App\Models\Konter;
use App\Models\Regency;
use App\Models\Village;
use App\Models\District;
use App\Models\Province;
use App\Http\Controllers\Controller;
use Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class KonterController extends Controller
{
    public function index()
    {
        $konters = DB::table('konters')->orderBy('id', 'desc')->paginate(5);
        return view('admin.pages.konter.index', compact('konters'));
    }

    public function getRegencies(Request $request)
    {
        $data['regencies'] = Regency::where("province_code", $request->province_code)
            ->get(["name", "code"]);
        return response()->json($data);
    }
    public function getDistricts(Request $request)
    {
        $data['districts'] = District::where("regency_code", $request->regency_code)
            ->get(["name", "code"]);
        return response()->json($data);
    }
    public function getVillages(Request $request)
    {
        $data['villages'] = Village::where("district_code", $request->district_code)
            ->get(["name", "code"]);
        return response()->json($data);
    }

    public function create()
    {
        $data['provinces'] = Province::get(["name", "code"]);

        return view('admin.pages.pos.create', $data);
    }

    public function store(Request $request)
    {
        $request->validate([

            'postalcode' => 'required|numeric',
            'province_code' => 'required|string',
            'regency_code' => 'required|string',
            'district_code' => 'required|string',
            'village_code' => 'required|string',
            'image' => 'nullable',
            'geojson' => 'nullable',
            'category' => 'nullable',

        ]);

        $imgName = null;
        $geojsonName = null;

        if ($request->image) {
            $imgName = time() . '-image.' . $request->image->getClientOriginalExtension();
            $request->image->move(public_path('konterImg'), $imgName);
        }

        if ($request->geojson) {
            $gJson =  time() . '-' . $request->postalcode . '-geojson.' . $request->geojson->getClientOriginalExtension();
            $request->geojson->move(public_path('geojson'), $gJson);

            $geojsonName = 'geojson/' . $gJson;
        }

        Konter::create([
            'postalcode' => $request->postalcode,
            'province_code' => $request->province_code,
            'regency_code' => $request->regency_code,
            'district_code' => $request->district_code,
            'village_code' => $request->village_code,
            'category' => $request->category,
            'image' => $imgName,
            'geojson' => $geojsonName,
        ]);


        notice('success', 'Data Added Successfully');
        return redirect()->route('back-office.pos.index');
    }

    public function edit($id)
    {
        $data['provinces'] = Province::get(["name", "code"]);

        $konters = Konter::findOrFail($id);
        return view('admin.pages.pos.edit', $data, compact('konters'));
    }


    public function update(Request $request, $id)
    {
        $request->validate([

            'postalcode' => 'required|string',
            'province_code' => 'required|string',
            'regency_code' => 'required|string',
            'district_code' => 'required|string',
            'village_code' => 'required|string',
            'image' => 'nullable',
            'geojson' => 'nullable',
            'category' => 'nullable',

        ]);

        $konter = Konter::find($id);

        $imgName = $konter->image;
        $geojsonName = $konter->geojson;

        if ($request->image) {
            $imgName = time() . '-image.' . $request->image->getClientOriginalExtension();
            $request->image->move(public_path('konterImg'), $imgName);
        }

        if ($request->geojson) {
            $gJson =  time() . '-' . $konter->postalcode . '-geojson.' . $request->geojson->getClientOriginalExtension();
            $request->geojson->move(public_path('geojson'), $gJson);

            $geojsonName = 'geojson/' . $gJson;
        }

        $konter->update([
            'postalcode' => $request->postalcode,
            'province_code' => $request->province_code,
            'regency_code' => $request->regency_code,
            'district_code' => $request->district_code,
            'village_code' => $request->village_code,
            'category' => $request->category,
            'image' => $imgName,
            'geojson' => $geojsonName,
        ]);

        notice('success', 'Data Updated Successfully');
        return redirect()->route('back-office.pos.index');
    }

    public function download($id)
    {
        $konters = Konter::find($id);
        $filePath = public_path() . "/" . $konters->geojson;
        $headers = ['Content-Type: application/json'];
        $fileName = $konters->geojson;

        return Response::download($filePath, $fileName, $headers);
    }

    public function destroy($id)
    {
        Konter::find($id)->delete();
        return redirect()->route('back-office.pos.index');
    }
}

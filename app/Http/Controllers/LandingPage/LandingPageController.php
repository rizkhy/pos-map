<?php

namespace App\Http\Controllers\LandingPage;

use App\Models\Konter;
use App\Models\Regency;
use App\Models\Village;
use App\Models\District;
use App\Models\Province;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Response;
use Illuminate\Support\Facades\DB;

class LandingPageController extends Controller
{
    public function __invoke(Request $request)
    {
        $data['provinces'] = Province::get(["name", "code"]);
        return view('front.pages.polygon.index', $data);
    }

    public function getRegencies(Request $request)
    {
        $data['regencies'] = Regency::where("province_code", $request->province_code)
            ->get(["name", "code"]);
        return response()->json($data);
    }

    public function getDistricts(Request $request)
    {
        $data['districts'] = District::where("regency_code", $request->regency_code)
            ->get(["name", "code"]);
        return response()->json($data);
    }

    public function getVillages(Request $request)
    {
        $data['villages'] = Village::where("district_code", $request->district_code)
            ->get(["name", "code"]);
        return response()->json($data);
    }

    public function getKonterGeoJSON(Request $request)
    {
        $konter = Konter::with('provinces', 'regency', 'district', 'village')->where("postalcode", $request->search)
            ->get();
        return response()->json($konter);
    }

    public function getProvGeoJSON(Request $request)
    {
        $data['provinces'] = Province::where("code", $request->code)
            ->get(["geojson"]);
        return response()->json($data);
    }

    public function getRegGeoJSON(Request $request)
    {
        $data['provinces'] = Regency::where("code", $request->code)
            ->get(["geojson"]);
        return response()->json($data);
    }

    public function getDisGeoJSON(Request $request)
    {
        $data['provinces'] = District::where("code", $request->code)
            ->get(["geojson"]);
        return response()->json($data);
    }

    public function getVilGeoJSON(Request $request)
    {
        $data['provinces'] = Village::where("code", $request->code)
            ->get(["geojson"]);
        return response()->json($data);
    }

    public function getPosCodeVill(Request $request)
    {
        $data['konters'] = Konter::with('village')->where("village_code",  $request->village_code)->get();
        // dd($data);
        return response()->json($data);
    }

    public function getUrlDownload(Request $request)
    {
        // dd($request->postalcode);
        $data['konters'] = Konter::where('postalcode', $request->postalcode)->get();
        return response()->json($data);
    }

    public function downloadImg(Request $request)
    {
        $konters = Konter::find($request->id);
        // dd($konters);
        $filePath = public_path() . "/konterImg/" . $konters->image;
        $headers = ['Content-Type: image/png'];
        $fileName = $konters->image;

        return Response::download($filePath, $fileName, $headers);
    }

    public function downloadImgVill(Request $request)
    {
        $konters = Konter::find($request->id);
        // dd($konters);
        $filePath = public_path() . "/konterImg/" . $konters->image;
        $headers = ['Content-Type: image/png'];
        $fileName = $konters->image;

        return Response::download($filePath, $fileName, $headers);
    }

    public function downloadImages($image)
    {
        $konters = Konter::find($image);
        $filePath = public_path() . "/konterImg/" . $konters->image;
        $headers = ['Content-Type: image/png'];
        $fileName = $konters->image;


        return Response::download($filePath, $fileName, $headers);
    }
}

<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Resources\Konter as ResourcesKonter;
use App\Models\Konter;

class MapPos extends Controller
{
    public function index(Request $request)
    {
        $konters = Konter::all();

        $geoJSONdata = $konters->map(function ($konter) {
            return [
                'type'       => 'Feature',
                'properties' => new ResourcesKonter($konter),
                'geometry'   => [
                    // 'type'        => 'Point',
                    'type'        => 'MultiPolygon',
                    'coordinates' => [
                        $konter->longitude,
                        $konter->latitude,
                    ],
                ],
            ];
        });

        return response()->json([
            'type'     => 'FeatureCollection',
            'features' => $geoJSONdata,
        ]);
    }
}

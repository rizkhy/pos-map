<?php

namespace App\Http\Controllers\Region;

use App\Models\Village;
use App\Models\Province;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class VillageController extends Controller
{
    public function create()
    {
        $provinces = Province::all();

        return view('admin.pages.region.partials.village.create', compact('provinces'));
    }

    public function store(Request $request)
    {
        $request->validate([

            'code' => 'required|numeric|digits_between:1,10',
            'name' => 'required|string',
            'district_code' => 'required',
            'geojson' => 'nullable',

        ]);

        $geojsonName = null;
        if (isset($request->geojson)) {
            $geojsonName =  time() . '-' . $request->code . '-village.' . $request->geojson->getClientOriginalExtension();
            $request->geojson->move(public_path('village'), $geojsonName);
        }

        Village::create([
            'code' => $request->code,
            'district_code' => $request->district_code,
            'name' => $request->name,
            'geojson' => 'village/' . $geojsonName,
        ]);

        notice('success', 'Data Added Successfully');
        return redirect()->route('back-office.region.village');
    }

    public function edit($id)
    {
        $provinces = Province::all();
        $village = Village::find($id);

        return view('admin.pages.region.partials.village.edit', compact('provinces', 'village'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([

            'code' => 'required|numeric|digits_between:1,10',
            'name' => 'required|string',
            'district_code' => 'required',
            'geojson' => 'nullable',

        ]);

        $village = Village::find($id);

        $geojsonName = $village->geojson;

        if (isset($request->geojson)) {
            $geojsonName =  time() . '-' . $request->code . '-village.' . $request->geojson->getClientOriginalExtension();
            $request->geojson->move(public_path('village'), $geojsonName);
        }

        $village->update([
            'code' => $request->code,
            'district_code' => $request->district_code,
            'name' => $request->name,
            'geojson' => 'village/' . $geojsonName,
        ]);

        // notice('success', 'Data Updated Successfully');
        return redirect()->route('back-office.region.village');
    }
}

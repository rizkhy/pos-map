<?php

namespace App\Http\Controllers\Region;

use App\Http\Controllers\Controller;
use App\Models\Province;
use Illuminate\Http\Request;

class ProvinceController extends Controller
{
    public function create()
    {
        return view('admin.pages.region.partials.province.create');
    }

    public function store(Request $request)
    {
        $request->validate([

            'code' => 'required|numeric|digits_between:1,2',
            'name' => 'required|string',
            'geojson' => 'nullable',

        ]);

        $geojsonName = null;
        if (isset($request->geojson)) {
            $geojsonName =  time() . '-' . $request->code . '-province.' . $request->geojson->getClientOriginalExtension();
            $request->geojson->move(public_path('province'), $geojsonName);
        }

        Province::create([
            'code' => $request->code,
            'name' => $request->name,
            'geojson' => 'province/' . $geojsonName,
        ]);

        notice('success', 'Data Added Successfully');
        return redirect()->route('back-office.region.province');
    }

    public function edit($id)
    {
        $province = Province::find($id);

        return view('admin.pages.region.partials.province.edit', compact('province'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([

            'code' => 'required|numeric|digits_between:1,2',
            'name' => 'required|string',
            'geojson' => 'nullable',

        ]);

        $province = Province::find($id);

        $geojsonName = $province->geojson;

        if (isset($request->geojson)) {
            $geojsonName =  time() . '-' . $request->code . '-province.' . $request->geojson->getClientOriginalExtension();
            $request->geojson->move(public_path('province'), $geojsonName);
        }

        $province->update([
            'code' => $request->code,
            'name' => $request->name,
            'geojson' => 'province/' . $geojsonName,
        ]);

        // notice('success', 'Data Updated Successfully');
        return redirect()->route('back-office.region.province');
    }
}

<?php

namespace App\Http\Controllers\Region;

use App\Http\Controllers\Controller;
use App\Models\Province;
use App\Models\Regency;
use Illuminate\Http\Request;

class RegencyController extends Controller
{
    public function create()
    {
        $provinces = Province::all();
        return view('admin.pages.region.partials.city.create', compact('provinces'));
    }

    public function store(Request $request)
    {
        $request->validate([

            'code' => 'required|numeric|digits_between:1,4',
            'name' => 'required|string',
            'province_code' => 'required',
            'geojson' => 'nullable',

        ]);

        $geojsonName = null;
        if (isset($request->geojson)) {
            $geojsonName =  time() . '-' . $request->code . '-regency.' . $request->geojson->getClientOriginalExtension();
            $request->geojson->move(public_path('regency'), $geojsonName);
        }

        Regency::create([
            'code' => $request->code,
            'province_code' => $request->province_code,
            'name' => $request->name,
            'geojson' => 'regency/' . $geojsonName,
        ]);

        notice('success', 'Data Added Successfully');
        return redirect()->route('back-office.region.city');
    }

    public function edit($id)
    {
        $provinces = Province::all();
        $regency = Regency::find($id);

        return view('admin.pages.region.partials.city.edit', compact('provinces', 'regency'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([

            'code' => 'required|numeric|digits_between:1,4',
            'name' => 'required|string',
            'province_code' => 'required',
            'geojson' => 'nullable',

        ]);

        $regency = Regency::find($id);

        $geojsonName = $regency->geojson;

        if (isset($request->geojson)) {
            $geojsonName =  time() . '-' . $request->code . '-regency.' . $request->geojson->getClientOriginalExtension();
            $request->geojson->move(public_path('regency'), $geojsonName);
        }

        $regency->update([
            'code' => $request->code,
            'province_code' => $request->province_code,
            'name' => $request->name,
            'geojson' => 'regency/' . $geojsonName,
        ]);

        // notice('success', 'Data Updated Successfully');
        return redirect()->route('back-office.region.city');
    }
}

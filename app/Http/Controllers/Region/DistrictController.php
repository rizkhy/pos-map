<?php

namespace App\Http\Controllers\Region;

use App\Models\District;
use App\Models\Province;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DistrictController extends Controller
{
    public function create()
    {
        $provinces = Province::all();

        return view('admin.pages.region.partials.district.create', compact('provinces'));
    }

    public function store(Request $request)
    {
        $request->validate([

            'code' => 'required|numeric|digits_between:1,7',
            'name' => 'required|string',
            'regency_code' => 'required',
            'geojson' => 'nullable',

        ]);

        $geojsonName = null;
        if (isset($request->geojson)) {
            $geojsonName =  time() . '-' . $request->code . '-district.' . $request->geojson->getClientOriginalExtension();
            $request->geojson->move(public_path('district'), $geojsonName);
        }

        District::create([
            'code' => $request->code,
            'regency_code' => $request->regency_code,
            'name' => $request->name,
            'geojson' => 'district/' . $geojsonName,
        ]);

        notice('success', 'Data Added Successfully');
        return redirect()->route('back-office.region.district');
    }

    public function edit($id)
    {
        $provinces = Province::all();
        $district = District::find($id);

        return view('admin.pages.region.partials.district.edit', compact('provinces', 'district'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([

            'code' => 'required|numeric|digits_between:1,7',
            'name' => 'required|string',
            'regency_code' => 'required',
            'geojson' => 'nullable',

        ]);

        $district = District::find($id);

        $geojsonName = $district->geojson;

        if (isset($request->geojson)) {
            $geojsonName =  time() . '-' . $request->code . '-district.' . $request->geojson->getClientOriginalExtension();
            $request->geojson->move(public_path('district'), $geojsonName);
        }

        $district->update([
            'code' => $request->code,
            'regency_code' => $request->regency_code,
            'name' => $request->name,
            'geojson' => 'district/' . $geojsonName,
        ]);

        // notice('success', 'Data Updated Successfully');
        return redirect()->route('back-office.region.district');
    }
}

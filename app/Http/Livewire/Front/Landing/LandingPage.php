<?php

namespace App\Http\Livewire\Front\Landing;

use App\Models\District;
use App\Models\Konter;
use App\Models\Province;
use App\Models\Regency;
use App\Models\Village;
use Livewire\Component;

class LandingPage extends Component
{
    public $province_code;
    public $regency_code;
    public $district_code;
    public $village_code;
    public $geojson;

    public function getProvincesProperty()
    {
        return Province::all();
    }

    public function getRegenciesProperty()
    {
        return Regency::where('province_code', $this->province_code)->get();
    }

    public function getDistrictsProperty()
    {
        return District::where('regency_code', $this->regency_code)->get();
    }

    public function getVillagesProperty()
    {
        return Village::where('district_code', $this->district_code)->get();
    }

    public function getKontersProperty()
    {
        return Konter::where('village_code', $this->geojson)->get();
    }

    public function hydrate()
    {
        $this->dispatchBrowserEvent('render');
    }

    public function render()
    {
        // dd($this->getKontersProperty());
        return view('front.pages.polygon.index')
            ->extends('front.layout.master')
            ->section('content');
    }
}

<?php

namespace App\Http\Livewire\Front\CoomingSoon;

use Livewire\Component;

class CoomingSoonPage extends Component
{
    public function render()
    {
        return view('front.pages.coomingsoon.cooming-soon')
            ->extends('front.layout.master')
            ->section('content');
    }
}

<?php

namespace App\Http\Livewire\Back\Setting;

use Livewire\Component;

class SettingPage extends Component
{
    public function render()
    {
        $page_title = 'Pengaturan';

        return view('admin.pages.setting.index')
            ->extends('admin.layout.master', compact('page_title'))
            ->section('content');
    }
}

<?php

namespace App\Http\Livewire\Back\Setting;

use Livewire\Component;

class EditSettingPage extends Component
{
    public function render()
    {
        $page_title = 'Pengaturan';

        return view('admin.pages.setting.edit')
            ->extends('admin.layout.master', compact('page_title'))
            ->section('content');
    }
}

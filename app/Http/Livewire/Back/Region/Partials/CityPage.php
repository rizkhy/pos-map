<?php

namespace App\Http\Livewire\Back\Region\Partials;

use App\Models\Province;
use App\Models\Regency;
use Livewire\Component;
use Livewire\WithPagination;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

class CityPage extends Component
{
    use WithPagination;

    public $search;
    public $codeProvince;
    public $provinces;

    protected $listeners = ['removed'];
    protected $paginationTheme = 'bootstrap';

    public function mount()
    {
        $this->provinces = Province::all();
    }

    public function getRegenciesProperty(): LengthAwarePaginator
    {
        return Regency::with('provinces')
            ->when(!empty($this->search), function ($q) {
                $q->where('name', 'like', "%{$this->search}%");
            })
            ->when(!empty($this->codeProvince), function ($q) {
                $q->where('province_code', $this->codeProvince);
            })
            ->orderBy('code')
            ->paginate();
    }

    public function deleteConfirm($id)
    {
        $this->dispatchBrowserEvent('swal:confirm', [
            'type' => 'warning',
            'message' => 'Are you sure?',
            'text' => 'Seluruh Data Dari Kab/Kota Hingga Kelurahan Akan Dihapus!',
            'id' => $id,
        ]);
    }

    public function removed($id)
    {
        // $item = Regency::findOrFail($id);
        $item = Regency::where('id', $id);

        if (isset($item->geojson)) {
            $file = public_path() . "/" . $item->geojson;

            unlink($file);
        }

        $item->delete();

        notice('success', 'Data Removed Successfully');
        $this->redirectRoute('back-office.region.city');
    }

    public function render()
    {
        $page_title = 'Kota';

        // dd($this->getRegenciesProperty());

        return view('admin.pages.region.partials.city.index')
            ->extends('admin.layout.master', compact('page_title'))
            ->section('content');
    }
}

<?php

namespace App\Http\Livewire\Back\Region\Partials;

use Livewire\Component;
use App\Models\Province;
use Livewire\WithPagination;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

class ProvincePage extends Component
{
    use WithPagination;

    public $search;
    protected $listeners = ['removed'];
    protected $paginationTheme = 'bootstrap';

    public function getProvincesProperty(): LengthAwarePaginator
    {
        return Province::when(!empty($this->search), function ($q) {
            $q->where('name', 'like', "%{$this->search}%");
        })
            ->orderBy('code')
            ->paginate();
    }

    public function deleteConfirm($id)
    {
        $this->dispatchBrowserEvent('swal:confirm', [
            'type' => 'warning',
            'message' => 'Are you sure?',
            'text' => 'Seluruh Data Dari Provinsi Hingga Kelurahan Akan Dihapus!',
            'id' => $id,
        ]);
    }

    public function removed($id)
    {
        // $item = Province::findOrFail($id);
        $item = Province::where('id', $id);

        if (isset($item->geojson)) {
            $file = public_path() . "/" . $item->geojson;

            unlink($file);
        }

        $item->delete();

        notice('success', 'Data Removed Successfully');
        $this->redirectRoute('back-office.region.province');
    }

    public function render()
    {
        $page_title = 'Provinsi';

        return view('admin.pages.region.partials.province.index')
            ->extends('admin.layout.master', compact('page_title'))
            ->section('content');
    }
}

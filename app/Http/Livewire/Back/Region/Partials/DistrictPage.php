<?php

namespace App\Http\Livewire\Back\Region\Partials;

use Livewire\Component;
use App\Models\District;
use App\Models\Province;
use App\Models\Regency;
use Livewire\WithPagination;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

class DistrictPage extends Component
{
    use WithPagination;

    public $search;
    public $provinces;
    public $codeProvince;
    public $codeCity;

    protected $listeners = ['removed'];
    protected $paginationTheme = 'bootstrap';

    public function mount()
    {
        $this->provinces = Province::all();
    }

    public function getRegenciesProperty()
    {
        return Regency::where('province_code', $this->codeProvince)->get();
    }

    public function getDistrictsProperty(): LengthAwarePaginator
    {
        return District::with('regency')
            ->when(!empty($this->search), function ($q) {
                $q->where('name', 'like', "%{$this->search}%");
            })
            ->whereHas('regency', function ($q) {
                $q->when(!empty($this->codeProvince), function ($q) {
                    $q->where('province_code', $this->codeProvince);
                });
            })
            ->when(!empty($this->codeCity), function ($q) {
                $q->where('regency_code', $this->codeCity);
            })
            ->orderBy('code')
            ->paginate();
    }

    public function deleteConfirm($id)
    {
        $this->dispatchBrowserEvent('swal:confirm', [
            'type' => 'warning',
            'message' => 'Are you sure?',
            'text' => 'Seluruh Data Dari Kecamatan Dan Kelurahan Akan Dihapus!',
            'id' => $id,
        ]);
    }

    public function removed($id)
    {
        // $item = District::findOrFail($id);
        $item = District::where('id', $id);

        if (isset($item->geojson)) {
            $file = public_path() . "/" . $item->geojson;

            unlink($file);
        }

        $item->delete();

        notice('success', 'Data Removed Successfully');
        $this->redirectRoute('back-office.region.district');
    }

    public function render()
    {
        $page_title = 'Kecamatan';

        return view('admin.pages.region.partials.district.index')
            ->extends('admin.layout.master', compact('page_title'))
            ->section('content');
    }
}

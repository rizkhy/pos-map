<?php

namespace App\Http\Livewire\Back\Region\Partials;

use App\Models\District;
use App\Models\Province;
use App\Models\Regency;
use App\Models\Village;
use Livewire\Component;
use Livewire\WithPagination;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

class VillagePage extends Component
{
    use WithPagination;

    public $search;
    public $provinces;
    public $codeProvince;
    public $codeCity;
    public $codeDistrict;

    protected $listeners = ['removed'];
    protected $paginationTheme = 'bootstrap';

    public function mount()
    {
        $this->provinces = Province::all();
    }

    public function getRegenciesProperty()
    {
        return Regency::where('province_code', $this->codeProvince)->get();
    }

    public function getDistrictsProperty()
    {
        return District::where('regency_code', $this->codeCity)->get();
    }

    public function getVillagesProperty(): LengthAwarePaginator
    {
        return Village::with('district')
            ->when(!empty($this->search), function ($q) {
                $q->where('name', 'like', "%{$this->search}%");
            })
            ->whereHas('district', function ($q) {
                $q->whereHas('regency', function ($q) {
                    $q->when(!empty($this->codeProvince), function ($q) {
                        $q->where('province_code', $this->codeProvince);
                    });
                })->when(!empty($this->codeCity), function ($q) {
                    $q->where('regency_code', $this->codeCity);
                });
            })
            ->when(!empty($this->codeDistrict), function ($q) {
                $q->where('district_code', $this->codeDistrict);
            })
            ->orderBy('code')
            ->paginate();
    }

    public function deleteConfirm($id)
    {
        $this->dispatchBrowserEvent('swal:confirm', [
            'type' => 'warning',
            'message' => 'Are you sure?',
            'text' => 'Data Kelurahan Akan Dihapus!',
            'id' => $id,
        ]);
    }

    public function removed($id)
    {
        // $item = Village::findOrFail($id);
        $item = Village::where('id', $id);

        if (isset($item->geojson)) {
            $file = public_path() . "/" . $item->geojson;

            unlink($file);
        }

        $item->delete();

        notice('success', 'Data Removed Successfully');
        $this->redirectRoute('back-office.region.village');
    }

    public function render()
    {
        $page_title = 'Kelurahan';

        return view('admin.pages.region.partials.village.index')
            ->extends('admin.layout.master', compact('page_title'))
            ->section('content');
    }
}

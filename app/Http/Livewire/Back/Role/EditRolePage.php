<?php

namespace App\Http\Livewire\Back\Role;

use Livewire\Component;
use App\Models\Role;
use App\Models\Permission;

class EditRolePage extends Component
{
    public $role;
    public $name;
    public $all;
    public $listPermission;
    public $permission = [];

    public function mount($id)
    {
        $this->role = Role::find($id);
        $this->name = $this->role->name;
        $this->listPermission = Permission::get()->toArray();
        foreach ($this->role->permissions as $key => $perm) {
            $this->permission[$perm->name] = [
                'name' => $perm->name
            ];
        }
        // dd($this->permission);
    }

    protected function rules(): array
    {
        return [
            'name' => 'required',
            'permission.*.name' => 'required',
        ];
    }

    public function checkAll()
    {
        if ($this->all == '') {
            $this->permission = [];
        } else {
            $this->permission = Permission::get(['name'])->toArray();
        }
    }

    public function saved()
    {
        $this->validate();

        $data['name'] = $this->name;
        $data['guard_name'] = 'web';
        $this->role->syncPermissions($this->permission);

        notice('success', 'Data Updated Successfully');
        $this->redirectRoute('back-office.role.index');
    }

    public function render()
    {
        $page_title = 'Role';

        return view('admin.pages.role.edit')
            ->extends('admin.layout.master', compact('page_title'))
            ->section('content');
    }
}

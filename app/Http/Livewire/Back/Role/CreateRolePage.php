<?php

namespace App\Http\Livewire\Back\Role;

use App\Models\Permission;
use App\Models\Role;
use Livewire\Component;

class CreateRolePage extends Component
{
    public $name;
    public $all;
    public $listPermission;
    public $permission = [];

    public function mount()
    {
        $this->listPermission = Permission::get()->toArray();
    }

    protected function rules(): array
    {
        return [
            'name' => 'required|unique:roles,name',
            'permission.*.name' => 'required',
        ];
    }

    public function checkAll()
    {
        if ($this->all == '') {
            $this->permission = [];
        } else {
            $this->permission = Permission::get(['name'])->toArray();
        }
    }

    public function saved()
    {
        $this->validate();

        $data['name'] = $this->name;
        $data['guard_name'] = 'web';
        $r = Role::create($data);
        $r->givePermissionTo($this->permission);

        notice('success', 'Data Added Successfully');
        $this->redirectRoute('back-office.role.index');
    }

    public function render()
    {
        $page_title = 'Role';

        return view('admin.pages.role.create')
            ->extends('admin.layout.master', compact('page_title'))
            ->section('content');
    }
}

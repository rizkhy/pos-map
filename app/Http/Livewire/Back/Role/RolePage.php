<?php

namespace App\Http\Livewire\Back\Role;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\Role;
// use Spatie\Permission\Models\Role;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

class RolePage extends Component
{
    use WithPagination;

    protected $listeners = ['removed'];
    protected $paginationTheme = 'bootstrap';

    public function getRolesProperty(): LengthAwarePaginator
    {
        return Role::latest()
            ->paginate();
    }

    public function deleteConfirm($id)
    {
        $this->dispatchBrowserEvent('swal:confirm', [
            'type' => 'warning',
            'message' => 'Are you sure?',
            'text' => 'Data Role Akan Dihapus!',
            'id' => $id,
        ]);
    }

    public function removed($id)
    {
        $role = Role::findOrFail($id);
        $role->delete();

        notice('success', 'Data Removed Successfully');
        $this->redirectRoute('back-office.role.index');
    }

    public function render()
    {
        $page_title = 'Role';

        return view('admin.pages.role.index')
            ->extends('admin.layout.master', compact('page_title'))
            ->section('content');
    }
}

<?php

namespace App\Http\Livewire\Back\Image;

use Livewire\Component;

class EditImagePage extends Component
{
    public function render()
    {
        $page_title = 'Gambar';

        return view('admin.pages.image.edit')
            ->extends('admin.layout.master', compact('page_title'))
            ->section('content');
    }
}

<?php

namespace App\Http\Livewire\Back\Image;

use Livewire\Component;

class ImagePage extends Component
{
    public function render()
    {
        $page_title = 'Gambar';

        return view('admin.pages.image.index')
            ->extends('admin.layout.master', compact('page_title'))
            ->section('content');
    }
}

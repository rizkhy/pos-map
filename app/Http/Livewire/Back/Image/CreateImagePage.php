<?php

namespace App\Http\Livewire\Back\Image;

use App\Models\Konter;
use Livewire\Component;
use Livewire\WithFileUploads;
use Illuminate\Http\Request;

class CreateImagePage extends Component
{

    use WithFileUploads;
    public $konter;
    public $otherimg = [];
    public $image;

    public function getKontersProperty()
    {
        return Konter::all();
    }

    public function add()
    {
        $otherimg = $this->otherimg;
        array_push($otherimg, [
            'otherimg' => null
        ]);

        $this->otherimg = $otherimg;
    }

    protected function rules(): array
    {
        return [
            'konter.id' => 'required',
            'konter.image' => 'image|file|required',
            'otherimg.*.otherimg' => 'required',

        ];
    }

    public function saved()
    {
        $this->validate();

        dd($this->validate());

        $this->konter->id = $this->id;

        $this->konter->image = $this->image->store('konters', 'public');

        foreach ($this->otherimg as $key => $value) {
            if (!is_null($value['otherimg'])) {

                $path =  $value['otherimg']->store('konters', 'public');

                $this->konter->otherimg()->create([
                    'otherimg' => $path
                ]);
            }
        }
        $this->konter->save();

        $this->redirectRoute('back-office.image.index');
    }

    public function render()
    {
        $page_title = 'Gambar';

        return view('admin.pages.image.create')
            ->extends('admin.layout.master', compact('page_title'))
            ->section('content');
    }
}

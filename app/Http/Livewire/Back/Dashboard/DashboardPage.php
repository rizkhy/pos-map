<?php

namespace App\Http\Livewire\Back\Dashboard;

use Livewire\Component;

class DashboardPage extends Component
{
    public function render()
    {
        $page_title = 'Dashboard';

        return view('admin.pages.dashboard.index')
            ->extends('admin.layout.master', compact('page_title'))
            ->section('content');
    }
}

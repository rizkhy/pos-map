<?php

namespace App\Http\Livewire\Back\User;

use App\Models\User;
use App\Models\Role;
use Livewire\Component;
// use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Hash;

class CreateUserPage extends Component
{
    public $user;
    public $password;
    public $roles;
    public $role;

    public function mount()
    {
        $this->user = new User;
        $this->roles = Role::all();
    }

    protected function rules(): array
    {
        return [
            'user.name' => 'required|string',
            'user.email' => 'required|email|unique:users,email',
            'password' => 'required|string',
            'role' => 'required',
        ];
    }

    public function saved()
    {
        $this->validate();

        $this->user->password = Hash::make($this->password);
        $this->user->save();

        $this->user->assignRole(Role::find($this->role));

        notice('success', 'Data Added Successfully');
        $this->redirectRoute('back-office.user.index');
    }

    public function render()
    {
        $page_title = 'Administrator';

        return view('admin.pages.user.create')
            ->extends('admin.layout.master', compact('page_title'))
            ->section('content');
    }
}

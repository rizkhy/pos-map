<?php

namespace App\Http\Livewire\Back\User;

use App\Models\User;
use App\Models\Role;
use Livewire\Component;
// use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Hash;

class EditUserPage extends Component
{
    public $user;
    public $password;
    public $roles;
    public $role;

    public function mount($id)
    {
        $this->user = User::find($id);
        $this->roles = Role::all();
        $this->role = $this->user->roles->pluck('id')->first();
    }

    protected function rules(): array
    {
        return [
            'user.name' => 'required|string',
            'user.email' => 'required|email|unique:users,email,' . $this->user->id,
            'password' => 'nullable|string',
            'role' => 'nullable',
        ];
    }

    public function saved()
    {
        $this->validate();

        if (!empty($this->password)) {
            $this->user->password = Hash::make($this->password);
        } else {
            $this->user->password = $this->user->password;
        }

        $this->user->save();

        if (isset($this->role)) {
            $this->user->syncRoles(Role::find($this->role));
        }

        notice('success', 'Data Updated Successfully');
        $this->redirectRoute('back-office.user.index');
    }

    public function render()
    {
        $page_title = 'Administrator';

        return view('admin.pages.user.edit')
            ->extends('admin.layout.master', compact('page_title'))
            ->section('content');
    }
}

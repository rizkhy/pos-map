<?php

namespace App\Http\Livewire\Back\User;

use App\Models\User;
use Livewire\Component;
use Livewire\WithPagination;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

class UserPage extends Component
{
    use WithPagination;

    protected $listeners = ['removed'];
    protected $paginationTheme = 'bootstrap';

    public function getUsersProperty(): LengthAwarePaginator
    {
        return User::latest()
            ->paginate();
    }

    public function deleteConfirm($id)
    {
        $this->dispatchBrowserEvent('swal:confirm', [
            'type' => 'warning',
            'message' => 'Are you sure?',
            'text' => 'Data User Akan Dihapus!',
            'id' => $id,
        ]);
    }

    public function removed($id)
    {
        User::findOrFail($id)->delete();

        notice('success', 'Data Removed Successfully');
        $this->redirectRoute('back-office.user.index');
    }

    public function render()
    {
        $page_title = 'Administrator';

        return view('admin.pages.user.index')
            ->extends('admin.layout.master', compact('page_title'))
            ->section('content');
    }
}

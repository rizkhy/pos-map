<?php

namespace App\Http\Livewire\Back\Login;

use App\Models\User;
use Auth;
use Hash;
use Livewire\Component;

class LoginPage extends Component
{
    public $email;
    public $password;

    protected $rules = [
        'email' => 'required|email|exists:users,email',
        'password' => 'required|string'
    ];

    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }

    public function doLogin()
    {
        $this->validate();

        $user = User::whereEmail($this->email)->first();

        if (!Hash::check($this->password, $user->password)) {
            return $this->addError('password', 'Selected Password is invalid.');
        }

        Auth::guard()->login($user);
        request()->session()->regenerate();
        return redirect()->intended('back-office');
    }

    public function render()
    {
        $page_title = 'Login Admin';

        return view('admin.pages.login.index')
            ->extends('admin.layout.auth-master', compact('page_title'))
            ->section('content');
    }
}

<?php

namespace App\Http\Livewire\Back\Pos;

use File;
use Response;
use App\Models\Konter;
use App\Models\Regency;
use App\Models\Village;
use Livewire\Component;
use App\Models\District;
use App\Models\Province;
use Livewire\WithPagination;
use Illuminate\Support\Facades\Storage;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

class PosPage extends Component
{
    use WithPagination;

    public $search;
    public $codeProvince;
    public $codeCity;
    public $codeDistrict;
    public $codeVillage;

    protected $listeners = ['removed'];
    protected $paginationTheme = 'bootstrap';

    public function getProvincesProperty()
    {
        return Province::all();
    }

    public function getRegenciesProperty()
    {
        return Regency::where('province_code', $this->codeProvince)->get();
    }

    public function getDistrictsProperty()
    {
        return District::where('regency_code', $this->codeCity)->get();
    }

    public function getVillagesProperty()
    {
        return Village::where('district_code', $this->codeDistrict)->get();
    }

    public function getKontersProperty(): LengthAwarePaginator
    {
        return Konter::when(!empty($this->search), function ($q) {
            $q->where('postalcode', 'like', "%{$this->search}%");
        })
            ->when(!empty($this->codeProvince), function ($q) {
                $q->where('province_code', $this->codeProvince);
            })
            ->when(!empty($this->codeCity), function ($q) {
                $q->where('regency_code', $this->codeCity);
            })
            ->when(!empty($this->codeDistrict), function ($q) {
                $q->where('district_code', $this->codeDistrict);
            })
            ->when(!empty($this->codeVillage), function ($q) {
                $q->where('village_code', $this->codeVillage);
            })
            ->latest()
            ->paginate();
    }

    public function deleteConfirm($id)
    {
        $this->dispatchBrowserEvent('swal:confirm', [
            'type' => 'warning',
            'message' => 'Are you sure?',
            'text' => 'If deleted, you will not be able to recover this imaginary file!',
            'id' => $id,
        ]);
    }

    public function removed($id)
    {
        // $item = Konter::findOrFail('id', $id);
        $item = Konter::where('id', $id);

        if (isset($item->image)) {
            $file = public_path() . '/konterImg/' . $item->image;

            unlink($file);
        }

        if (isset($item->geojson)) {
            $file = public_path() . "/" . $item->geojson;

            unlink($file);
        }

        $item->delete();

        notice('success', 'Data Removed Successfully');
        $this->redirectRoute('back-office.pos.index');
    }

    public function download($id)
    {
        $konters = Konter::find($id);
        $filePath = public_path() . "/" . $konters->geojson;
        $headers = ['Content-Type: application/json'];
        $fileName = time() . '.json';

        return Response::download($filePath, $fileName, $headers);
    }

    public function render()
    {
        $page_title = 'Pos';

        return view('admin.pages.pos.index')
            ->extends('admin.layout.master', compact('page_title'))
            ->section('content');
    }
}

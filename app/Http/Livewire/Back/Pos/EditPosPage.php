<?php

namespace App\Http\Livewire\Back\Pos;

use App\Models\Konter;
use Livewire\Component;
use App\Models\District;
use App\Models\Province;
use App\Models\Regency;
use App\Models\Village;

class EditPosPage extends Component
{
    public $konter;
    public $name;
    public $nowhatsapp;
    public $postalcode;
    public $phone;
    public $province_code;
    public $regency_code;
    public $district_code;
    public $village_code;
    public $addsress;
    public $latitude;
    public $longitude;

    public function mount($id)
    {
        $this->konter = Konter::find($id);
        $this->name = $this->konter->name;
        $this->nowhatsapp = $this->konter->nowhatsapp;
        $this->postalcode = $this->konter->postalcode;
        $this->phone = $this->konter->phone;
        $this->province_code = Province::where('code', $this->konter->province_code)->get();
        $this->regency_code = Regency::where('code', $this->konter->regency_code)->get();
        $this->district_code = District::where('code', $this->konter->district_code)->get();
        $this->village_code = Village::where('code', $this->konter->village_code)->get();
        $this->addsress = $this->konter->addsress;
        $this->latitude = $this->konter->latitude;
        $this->longitude = $this->konter->longitude;
    }

    protected function rules(): array
    {
        return [
            'konter.name' => 'required|string',
            'konter.nowhatsapp' => 'nullable|string',
            'konter.postalcode' => 'required|string',
            'konter.phone' => 'nullable|string',
            'province_code' => 'required|string',
            'regency_code' => 'required|string',
            'district_code' => 'required|string',
            'village_code' => 'required|string',
            'konter.address' => 'nullable|string',
            'konter.latitude' => 'required|string',
            'konter.longitude' => 'required|string',
        ];
    }

    public function getProvincesProperty()
    {
        return Province::all();
    }

    public function getRegenciesProperty()
    {
        return Regency::where('province_code', $this->province_code)->get();
    }

    public function getDistrictsProperty()
    {
        return District::where('regency_code', $this->regency_code)->get();
    }

    public function getVillagesProperty()
    {
        return Village::where('district_code', $this->district_code)->get();
    }

    public function hydrate()
    {
        $this->dispatchBrowserEvent('render');
    }

    public function saved()
    {
        $this->validate();

        $this->konter->province_code = $this->province_code;
        $this->konter->regency_code = $this->regency_code;
        $this->konter->district_code = $this->district_code;
        $this->konter->village_code = $this->village_code;

        $this->konter->save();

        $this->redirectRoute('back-office.pos.index');
    }
    public function render()
    {
        $page_title = 'Konter';

        return view('admin.pages.pos.edit')
            ->extends('admin.layout.master', compact('page_title'))
            ->section('content');
    }
}

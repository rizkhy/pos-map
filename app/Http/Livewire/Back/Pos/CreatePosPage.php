<?php

namespace App\Http\Livewire\Back\Pos;

use App\Models\Konter;
use App\Models\Regency;
use App\Models\Village;
use Illuminate\Http\Request;
use Livewire\Component;
use App\Models\District;
use App\Models\Province;
use Livewire\WithFileUploads;
use Illuminate\Support\Facades\Storage;

class CreatePosPage extends Component
{

    // use WithFileUploads;
    public $konter;
    public $province_code;
    public $regency_code;
    public $district_code;
    public $village_code;
    public $image;
    public $geojson;

    public function mount()
    {
        $this->konter = new Konter();
    }

    protected function rules(): array
    {
        return [
            'konter.postalcode' => 'required|string',
            'province_code' => 'required|string',
            'regency_code' => 'required|string',
            'district_code' => 'required|string',
            'village_code' => 'required|string',
            'image' => 'nullable',
            'geojson' => 'nullable',
        ];
    }

    public function getProvincesProperty()
    {
        return Province::all();
    }

    public function getRegenciesProperty()
    {
        return Regency::where('province_code', $this->province_code)->get();
    }

    public function getDistrictsProperty()
    {
        return District::where('regency_code', $this->regency_code)->get();
    }

    public function getVillagesProperty()
    {
        return Village::where('district_code', $this->district_code)->get();
    }

    public function hydrate()
    {
        $this->dispatchBrowserEvent('render');
    }

    public function updated()
    {
        // $name   = $this->image->getClientOriginalName();
        // $mimes   = $this->image->getClientOriginalExtension();
        // dd($propertyName, $val, $mimes,);
        // $f = Storage::putFile('avatars', $this->geojson);
        // dd($f);
        // $geojsonName   = 'geojson' . '/' . $this->geojson->getClientOriginalName();
        // Storage::disk('local')->put($geojsonName, 'public');

        // $f = \File::make('geojson', 'json_data')->disk('public')
        //     ->storeAs($this->geojson->getClientOriginalName());
        // dd($this->geojson->move('geojson', $this->geojson->getClientOriginalName()));

    }

    public function saved(Request $request)
    {
        // ddd($this->validate());
        // dd($request->image);
        $this->validate();
        // dd($request->file('geojson'));
        // $geojsonName = Storage::putFile('avatars', $this->geojson->getClientOriginalName());
        // $file = $request->file('geojson');
        // $file->move('geojson', $file->getClientOriginalName());

        $this->konter->province_code = $this->province_code;
        $this->konter->regency_code = $this->regency_code;
        $this->konter->district_code = $this->district_code;
        $this->konter->village_code = $this->village_code;

        // $thumbnail   = $this->image->getClientOriginalName();

        // $this->konter->image = $this->image->store('konters', 'public');
        // $this->konter->geojson = $geojsonName;

        $this->konter->save();
        session()->flash('message', 'File successfully Uploaded.');

        // dd(Storage::get($this->konter->geojson));

        $this->redirectRoute('back-office.pos.index');
    }

    public function render()
    {
        $page_title = 'Konter';

        return view('admin.pages.pos.create')
            ->extends('admin.layout.master', compact('page_title'))
            ->section('content');
    }
}

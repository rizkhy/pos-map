<?php

namespace App\Models;

use App\Traits\HasUuid;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Konter extends Model
{
    use HasUuid;

    protected $fillable = [
        'postalcode',
        'province_code',
        'regency_code',
        'district_code',
        'village_code',
        'category',
        'image',
        'geojson',
    ];

    public function scopeFilter($query, array $filters)
    {
        $query->when($filters['search'] ?? false, function ($query, $search) {
            return $query->where('province_code', 'like', '%' . $search . '%')
                ->orWhere('postalcode', 'like', '%' . $search . '%');
        });
    }

    public function provinces()
    {
        return $this->belongsTo(Province::class, 'province_code', 'code');
    }

    public function regency()
    {
        return $this->belongsTo(Regency::class, 'regency_code', 'code');
    }

    public function district()
    {
        return $this->belongsTo(District::class, 'district_code', 'code');
    }

    public function village()
    {
        return $this->belongsTo(Village::class, 'village_code', 'code');
    }
}

<?php

/*
 * This file is part of the IndoRegion package.
 *
 * (c) Azis Hapidin <azishapidin.com | azishapidin@gmail.com>
 *
 */

namespace App\Models;

use App\Traits\HasUuid;
use AzisHapidin\IndoRegion\Traits\ProvinceTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Province Model.
 */
class Province extends Model
{
    use HasUuid;
    /**
     * Table name.
     *
     * @var string
     */
    protected $table = 'provinces';
    protected $fillable = ['code', 'id', 'name', 'geojson'];

    /**
     * Province has many regencies.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function regencies(): HasMany
    {
        return $this->hasMany(Regency::class, 'province_code', 'code');
    }

    public function konters()
    {
        return $this->hasMany(Konter::class, 'province_code');
    }
}

<?php

/*
 * This file is part of the IndoRegion package.
 *
 * (c) Azis Hapidin <azishapidin.com | azishapidin@gmail.com>
 *
 */

namespace App\Models;

use AzisHapidin\IndoRegion\Traits\VillageTrait;
use Illuminate\Database\Eloquent\Model;
use App\Models\District;
use App\Traits\HasUuid;

/**
 * Village Model.
 */
class Village extends Model
{
    use HasUuid;

    /**
     * Table name.
     *
     * @var string
     */
    protected $table = 'villages';
    protected $fillable = ['district_code', 'code', 'name', 'geojson'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'district_id'
    ];

    /**
     * Village belongs to District.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function district()
    {
        return $this->belongsTo(District::class, 'district_code', 'code');
    }

    public function konters()
    {
        return $this->hasMany(Konter::class, 'village_code');
    }
}

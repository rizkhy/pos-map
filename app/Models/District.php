<?php

/*
 * This file is part of the IndoRegion package.
 *
 * (c) Azis Hapidin <azishapidin.com | azishapidin@gmail.com>
 *
 */

namespace App\Models;

use AzisHapidin\IndoRegion\Traits\DistrictTrait;
use Illuminate\Database\Eloquent\Model;
use App\Models\Regency;
use App\Models\Village;
use App\Traits\HasUuid;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * District Model.
 */
class District extends Model
{
    use HasUuid;

    /**
     * Table name.
     *
     * @var string
     */
    protected $table = 'districts';
    protected $fillable = ['regency_code', 'code', 'name', 'geojson'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'regency_id'
    ];

    /**
     * District belongs to Regency.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function regency()
    {
        return $this->belongsTo(Regency::class, 'regency_code', 'code');
    }

    /**
     * District has many villages.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function villages()
    {
        return $this->hasMany(Village::class, 'district_code', 'code');
    }

    public function konters()
    {
        return $this->hasMany(Konter::class, 'district_code');
    }
}

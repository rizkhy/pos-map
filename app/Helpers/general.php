<?php

if (!function_exists('notice')) {
    function notice($labelClass, $content)
    {
        $notices = Session::get('message');
        if (!is_array($notices))
            $notices = [];

        array_push($notices, [
            'labelClass' => $labelClass,
            'content' => $content
        ]);

        Session::put('notice', $notices);
    }
}
